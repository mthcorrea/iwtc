var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

var options = {
    onKeyPress: function (cpf, ev, el, op) {
        var masks = ['000.000.000-000', '00.000.000/0000-00'],
            mask = (cpf.length > 14) ? masks[1] : masks[0];
        el.mask(mask, op);
    }
};

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
	$(".fancybox").fancybox({});
    $(".thumbs").simpleGal({
        mainImage: ".selected-image"
    });

    $('.mask-cpf-cnpj').mask('000.000.000-000', options);
    $('.mask-phone').mask(SPMaskBehavior, spOptions);
});

$("form").on("submit", function (e) {
    var $button = $(this).find('button[data-loading-text]');

    if ($button.length) {
        $($button).button("loading");
    }
});

if ( $(window).width() > 100) {
var mywindow = $(window);
var mypos = mywindow.scrollTop();
var up = false;
var newscroll;
mywindow.scroll(function () {
    newscroll = mywindow.scrollTop();
    if (newscroll > mypos && !up) {
        $('.navcustom').stop().fadeOut(730);
        up = !up;
        console.log(up);
    } else if(newscroll < mypos && up) {
        $('.navcustom').stop().fadeIn(300);
        up = !up;
    }
    mypos = newscroll;
});
}
