$(document).ready(function() {
    outdatedBrowser({
        bgColor: '#9fcf67',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: 'assets/js/outdatedbrowser/lang/pt-br.html'
    })
})
