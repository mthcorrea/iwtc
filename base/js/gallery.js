/*
 * simpleGal -v0.0.1
 * A simple image gallery plugin.
 * https://github.com/steverydz/simpleGal
 *
 * Made by Steve Rydz
 * Under MIT License
 */
! function(a) {
    a.fn.extend({
        simpleGal: function(b) {
            var c = {
                mainImage: ".placeholder"
            };
            return b = a.extend(c, b), this.each(function() {
                var c = a(this).find("a"),
                    d = a(this).siblings().find(b.mainImage);
                    c.first().addClass('active');
                c.on("click", function(b) {
                    b.preventDefault();
                    a(this).parent().find("a").removeClass('active');
                    a(this).addClass('active');
                    var c = a(this).attr("href");
                    d.animate({
                        opacity: 0.3,
                    }, 200, function() {
                        d.animate({
                            opacity: 1,
                        });
                        d.attr("src", c);
                        d.parent().attr("href", c);
                    });
                })
            })
        }
    })
}(jQuery);
