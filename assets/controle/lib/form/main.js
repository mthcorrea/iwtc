require.config({
  baseUrl: BASE+"assets/controle/lib/form/lib/"
  , shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'underscore': {
      exports: '_'
    },
    'bootstrap': {
      deps: ['jquery'],
      exports: '$.fn.popover'
    }
  }
  , paths: {
    app         : BASE+"assets/controle/lib/form/"
    , collections : BASE+"assets/controle/lib/form/collections"
    , data        : BASE+"assets/controle/lib/form/data"
    , models      : BASE+"assets/controle/lib/form/models"
    , helper      : BASE+"assets/controle/lib/form/helper"
    , templates   : BASE+"assets/controle/lib/form/templates"
    , views       : BASE+"assets/controle/lib/form/views"
  }
});
require([ 'app/app'], function(app){
  app.initialize();
});
