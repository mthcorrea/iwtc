﻿/*
 Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.html or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default", {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
    templates: [{
        title: "Duas colunas iguais",
        image: "2-col-sm-6.gif",
        description: "Insere duas colunas do mesmo tamanho no editor.",
        html: '<div class="row"><div class="col-sm-6"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-6"><p>Insira o conteúdo aqui.</p></div></div>'
    }, {
        title: "Três colunas iguais",
        image: "3-col-sm-4.gif",
        description: "Insere três colunas do mesmo tamanho no editor.",
        html: '<div class="row"><div class="col-sm-4"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-4"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-4"><p>Insira o conteúdo aqui.</p></div></div>'
    }, {
        title: "Quatro colunas iguais",
        image: "4-col-sm-3.gif",
        description: "Insere quatro colunas do mesmo tamanho no editor.",
        html: '<div class="row"><div class="col-sm-3"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-3"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-3"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-3"><p>Insira o conteúdo aqui.</p></div></div>'
    }, {
        title: "Duas colunas (67% e 33%)",
        image: "2-col-sm-8-col-sm-4.gif",
        description: "Insere duas colunas no editor, uma com a largura 67% do tamanho do conteúdo e outra com largura 33% do conteúdo.",
        html: '<div class="row"><div class="col-sm-8"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-4"><p>Insira o conteúdo aqui.</p></div></div>'
    }, {
        title: "Duas colunas (33% e 67%)",
        image: "2-col-sm-4-col-sm-8.gif",
        description: "Insere duas colunas no editor, uma com a largura 33% do tamanho do conteúdo e outra com largura 67% do conteúdo.",
        html: '<div class="row"><div class="col-sm-4"><p>Insira o conteúdo aqui.</p></div><div class="col-sm-8"><p>Insira o conteúdo aqui.</p></div></div>'
    }]
});