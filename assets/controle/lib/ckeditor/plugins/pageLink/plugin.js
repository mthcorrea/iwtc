CKEDITOR.plugins.add('pageLink',
{
    init: function(editor)
    {
        editor.addCommand('pageLinkDialog', new CKEDITOR.dialogCommand('pageLinkDialog'));
        editor.ui.addButton('PageLink',
        {
            label: 'Inserir Link para Páginas Internas',
            command: 'pageLinkDialog',
            icon: this.path+'images/icon.png'
        });
        CKEDITOR.dialog.add('pageLinkDialog', function(editor)
        {
            function atualiza_options(element, url) {
                var dialog = CKEDITOR.dialog.getCurrent();
                var select = dialog.getContentElement("general", element);
                select.clear();
                select.add("Carregando...", 1);
                $.getJSON(BASE + url, function (data) {
                    select.clear();
                    $.each(data, function (i, obj) {
                        select.add(obj.titulo, i);
                    });
                });
            }
            return {
                title : 'Propriedades do Link',
                minWidth : 400,
                minHeight : 200,
                contents :
                [
                    {
                        id : 'general',
                        label : 'Settings',
                        elements : [{
                            type : 'html',
                            html : 'Selecione a página que será aberta ao clicar no link.'
                        },
                        {
                            type : 'text',
                            id : 'contents',
                            label : 'Título do Link',
                            commit : function(data)
                            {
                                data.contents = this.getValue();
                            }
                        },
                        {
                            type : 'select',
                            id : 'url',
                            label : 'Página do Site',
                            "default": '/',
                            items : [['Home', '/']],
                            commit : function(data)
                            {
                                data.url = this.getValue();
                            }
                        },
                        {
                            type : 'select',
                            id : 'target',
                            label : 'Target',
                            "default": '_self',
                            items : [['Mesma Página', '_self'],['Nova Página', '_blank']],
                            commit : function( data )
                            {
                                data.target = this.getValue();
                            }
                        }]
                    }
                ],
                onShow: function()
                {
                    atualiza_options("url", "controle/api/menus");
                },
                onOk : function()
                {
                    var dialog = this,
                        data   = {},
                        link   = editor.document.createElement('a');
                    this.commitContent(data);
                    link.setAttribute('href', data.url);
                    link.setAttribute('target', data.target);
                    link.setHtml(data.contents);
                    editor.insertElement(link);
                }
            };
        });
    }
});