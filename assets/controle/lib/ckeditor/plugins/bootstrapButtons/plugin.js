CKEDITOR.plugins.add('bootstrapButtons', 
{
    init: function(editor)
    {
        var imagesPath = this.path + 'images/';
        editor.addCommand('bootstrapButtonsDialog', new CKEDITOR.dialogCommand('bootstrapButtonsDialog'));
        editor.ui.addButton('BootstrapButtons',
        {
            label: 'Botões',
            command: 'bootstrapButtonsDialog',
            icon: imagesPath + 'icon.png'
        });
        CKEDITOR.dialog.add('bootstrapButtonsDialog', function (editor)
        {
            var plugin = CKEDITOR.plugins.get("dynamicBlocks");
            //CKEDITOR.document.appendStyleSheet(CKEDITOR.getUrl(plugin.path + "dialogs/bootstrapButtons.css"));
            var config = editor.config;
            return {
                title: 'Botões',
                minWidth: 400,
                minHeight: 250,
                contents: 
                [
                    {
                        id : 'geral',
                        label: 'Botões',
                        elements: 
                        [
                            {
                                type: 'html',
                                html: 'Insira um botão personalizado no conteúdo da página.'
                            },
                            {
                                type : 'text',
                                id : 'btn_label',
                                label : 'Texto do Botão',
                                validate : CKEDITOR.dialog.validate.notEmpty('O botão deve conter um texto.'),
                                required : true,
                                commit : function (data)
                                {
                                    data.label = this.getValue();
                                }
                            },
                            {
                                type : 'text',
                                id : 'btn_url',
                                label : 'URL',
                                validate : CKEDITOR.dialog.validate.notEmpty('O botão deve conter uma URL.'),
                                required : true,
                                commit : function (data)
                                {
                                    data.url = this.getValue();
                                }
                            },
                            {
                                type : 'select',
                                id : 'btn_class',
                                label : 'Estilo do Botão',
                                items : 
                                [
                                    [ 'Padrão', 'btn-default' ],
                                    [ 'Principal', 'btn-primary' ],
                                    [ 'Perigo', 'btn-danger' ],
                                    [ 'Sucesso', 'btn-success' ],
                                    [ 'Informativo', 'btn-info' ]
                                ],
                                validate : CKEDITOR.dialog.validate.notEmpty('Defina um estilo para o botão.'),
                                required : true,
                                commit : function (data)
                                {
                                    data.class = this.getValue();
                                }
                            },
                            {
                                    type : 'checkbox',
                                    id : 'btn_target',
                                    label : 'Abrir em uma nova aba/janela',
                                    'default' : false,
                                    commit : function (data)
                                    {
                                        data.newPage = this.getValue();
                                    }
                            }
                        ]
                    }
                ],
                onOk: function()
                {
                    var dialog = this,
                        data = {},
                        link = editor.document.createElement('a');
                    this.commitContent(data);
                    link.setAttribute('href', data.url);
                    link.setAttribute('class', 'btn ' + data.class);
                    if (data.newPage)
                        link.setAttribute('target', '_blank');
                    link.setHtml(data.label);
                    editor.insertElement(link);
                }
            }
        });
    }
});