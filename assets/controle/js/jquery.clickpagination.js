(function( $ ){
 $.fn.clickPagination = function(options) {
		var opts = $.extend($.fn.clickPagination.defaults, options);  
		var target = opts.contentTarget;
	 	
		if ($(target).length == 0)
		{
			return false;
		}

		opts.contentTarget = target;

		return this.each(function() {
		  $.fn.clickPagination.init($(this), opts);
		});

  };

  $.fn.clickPagination.stopClickPagination = function(obj, opts){
  	   $(obj).attr('disabled', 'disabled');
 	   $(obj).html('Não há mais itens');
  }
  
  $.fn.clickPagination.loadContent = function(obj, opts){
	 var target = opts.contentTarget;

	 /* atualiza o contador da pagina */
	 opts.contentData.pag++;

	 if (opts.beforeLoad != null){
		opts.beforeLoad(); 
	 }
	 else
	 {
	 	$(obj).html('Carregando..');
	 }

	 $.ajax({
		  type: 'POST',
		  url: opts.contentPage,
		  data: opts.contentData,
		  success: function(data){

		  	if ( $.trim(data) != '') {

		  		var div = document.createElement('div');
				div.innerHTML = data;

				$(div).find('li').each(function (index, element) {
					var categoria_id = parseInt($(this).data('categoria-id'));
					var categoria_titulo = $(this).data('categoria-titulo');

					if (categoria_id > 0)
					{	
						if ($('.categoria'+categoria_id).length > 0)
						{
							$('.categoria'+categoria_id).append($(this));
						}
						else
						{
							$(target).append('</ul><div class="panel-heading"><h3 class="panel-subtitle">'+categoria_titulo+'</h3></div><ul class="table table-hover sortable categoria'+categoria_id+'"></ul>');
							$('.categoria'+categoria_id).append($(this));
						}
					}
					else
					{
						if ($('.categoria0').length > 0)
						{
							$('.categoria0').append($(this)); 
						}
						else
						{
							$(target).append($(this)); 
						}
						
					}
				});

				/* qtde de registros na página */
				var porPagina = $('.listar').find('li').length;
				$('.por_pagina').html(porPagina);

				/* sortable */
				sortable();

				/* texto do botão */
				$(obj).html(opts.textButton);
				
				if (opts.afterLoad != null)
				{
					var objectsRendered = $(target).children('[rel!=loaded]');
					opts.afterLoad(objectsRendered);	
				}

				if (porPagina >= opts.totalItens)
				{
					$.fn.clickPagination.stopClickPagination(obj, opts);
				}

		  	} else {
		  		$.fn.clickPagination.stopClickPagination(obj, opts);
		  	}
		  	
		  },
		  dataType: 'html'
	 });
	 
  };
  
  $.fn.clickPagination.init = function(obj, opts){
  	 opts.textButton = $(obj).html();

  	 opts.contentData.pag = 1;
  	 opts.totalItens = $('.total_itens').html();

	 $(obj).click(function(e) {
	 	e.preventDefault();

	 	if ($(obj).attr('disabled') != 'disabled') {
	 		
	 		$.fn.clickPagination.loadContent(obj, opts);		
		}
		else 
		{
			event.stopPropagation();	
		}
	 });
 };


 $.fn.clickPagination.defaults = {
      	 'contentPage' : '',
     	 'contentData' : {},
     	 'contentTarget': null,
		 'textButton': null,
		 'beforeLoad': null,
		 'afterLoad': null	,
 };	
})( jQuery );