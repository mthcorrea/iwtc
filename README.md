# Controle Padrão


## Vamos começar
Para trabalhar no projeto você vai precisar:

* IDE PHP
* PHP >= 7.1
* MySQL >= 5.7
* [Composer](https://getcomposer.org/)
* [Node JS](https://nodejs.org/en/)
* [Oh-My-Zsh](https://github.com/robbyrussell/oh-my-zsh/wiki/Installing-ZSH) (Terminal MAC)
* [Babun](http://babun.github.io/) (Terminal Windows)
* [Git Flow](https://danielkummer.github.io/git-flow-cheatsheet/index.pt_BR.html)

### Instalação
1 - Em seu terminal, execute no seu terminal:

```
git clone git@bitbucket.org:agenciarevelare/controle-padrao.git
```

2 - Feito o download do projeto, acesse a pasta do projeto.

3 - Crie o Virtual Host do seu projeto:

* [Virtual Host Windows utilizando Xampp](http://www.pauloacosta.com/2016/07/criando-multiplos-virtual-hosts-no-xampp/) 
* [Virtual Host Mac OS](https://coolestguidesontheplanet.com/how-to-set-up-virtual-hosts-in-apache-on-macos-osx-sierra/) 

3 - Agora execute no seu terminal:

```
composer install 
```
```
npm install
```

4 - Após as execuções:

* Crie o banco de dados. 
* Crie seu arquivo **db.php** dentro do diretório **fuel/app/config/development**.
* Execute no seu terminal:

```
php oil refine migrate
```

5 - Para realizar manutenções e/ou ver o visual do site em seu ambiente de desenvolvimento, execute em seu terminal:

```
gulp
```
**ou**
```
gulp watch
```

*Todos os arquivos e pastas gerados automáticamente no projeto, estão destacados no **.gitignore**. Assim é necessário utilizar um dos dois comandos destacados acima.*

6- Por fim, certifique-se:

* Verifique se no no diretório **assets/** está criado a pasta **cache**, caso não esteja criá-la.
* Verifique se no no diretório **fuel/app/** estão criadas as pastas **cache** e **tmp**, caso não estejam criá-las.

## Desenvolvimento

### Server-side

* [FuelPHP](https://fuelphp.com/)

### Client-side

* [Sass](http://sass-lang.com/)
* [jQuery](https://jquery.com/)
* [Bootstrap](https://getbootstrap.com/)

## Autor

[Agência Revelare](http://www.revelare.com.br/) 
