<?php
return array(
	'geral' => array(),
	'recaptcha' => array(
		'test' => array(
			'sitekey' => '6Lf7NmIUAAAAAG1vNltzL1Dp34ThyNO1y6qagGpV',
			'secretkey' => '6Lf7NmIUAAAAADhYRcxcmwgSdNdIp8QdwwoSDEil',
		),
		'production' => array(
			'sitekey' => '',
			'secretkey' => '',
		),
	),

    'empresa' => 'pao_criacao',
    'ssl' => false,
    'lang' => array(
        'pt_br' => 'Português',
        'en' => 'Inglês',
    ),
    'cliente_de' => array(
        'revelare' => array('titulo' => 'Revelare', 'telefones' => array('(14) 3879-6332', '/ 3204-0819'), 'logo' => 'controle::revelare.png'),
        'pao_criacao' => array('titulo' => 'Pão criação', 'telefones' => array('(14) 3313-9205'), 'logo' => 'controle::pao.png'),
    ),
);
