<?php
return array(
	'_root_' => 'frontend/index',
	'_404_'  => 'frontend/404',

    'controle'             => 'controle/inicial/',

	/* ARQUIVOS */
	'controle/arquivo'  => 'arquivo/controle/index',
	'controle/arquivo/(:any)' => 'arquivo/controle/$1',

	/* BANNERS */
	'controle/banner'  => 'banner/controle/index',
	'controle/banner/(:any)' => 'banner/controle/$1',

	/* CONFIGURAÇÃO */
	'controle/configuracao' => 'configuracao/controle/index',
	'controle/configuracao/(:any)' => 'configuracao/controle/$1',

	/* CONTATOS */
	'controle/contato' => 'contato/controle/index',
	'controle/contato/(:any)' => 'contato/controle/$1',

	/* MÍDIAS */
	'controle/midia' => 'midia/controle/index',
	'controle/midia/(:any)' => 'midia/controle/$1',

	/* CATEGORIAS DE OBRAS */
	'controle/obra/categoria'  => 'obra/categoria/controle/index',
	'controle/obra/categoria/(:any)' => 'obra/categoria/controle/$1',

	/* SUBCATEGORIAS DE OBRAS */
	'controle/obra/subcategoria'  => 'obra/subcategoria/controle/index',
	'controle/obra/subcategoria/(:any)' => 'obra/subcategoria/controle/$1',

	/* OBRAS */
	'controle/obra'  => 'obra/controle/index',
	'controle/obra/(:any)' => 'obra/controle/$1',

	/* PRODUTOS */
	'controle/produto'  => 'produto/controle/index',
	'controle/produto/(:any)' => 'produto/controle/$1',

	/* POSTAGENS */
	'controle/postagem'  => 'postagem/controle/index',
	'controle/postagem/(:any)' => 'postagem/controle/$1',

    /* USUÁRIOS */
    'controle/usuario'  => 'usuario/controle/index',
    'controle/usuario/(:any)' => 'usuario/controle/$1',

	/* VAGAS */
	'controle/vaga'  => 'vaga/controle/index',
	'controle/vaga/(:any)' => 'vaga/controle/$1',

    'controle/login'      => 'usuario/controle/login',
    'controle/logout'     => 'usuario/controle/logout',
    'controle/minhaconta' => 'usuario/controle/minhaconta',
    'controle/recuperarsenha'         => 'usuario/controle/recuperarsenha',
    'controle/recuperarsenha/(:hash)' => 'usuario/controle/recuperarsenha/alterarsenha/$1',

    'controle/(:any)' => 'controle/$1',

    /* SITE */
    'a-sendi' => 'frontend/sendi',

	'pre-fabricados' => 'frontend/fabricados',
    'pre-fabricados/sobre' => 'frontend/fabricados_sobre',
    'pre-fabricados/produtos' => 'frontend/fabricados_produtos',
    'pre-fabricados/produtos/(:any)' => 'frontend/fabricados_produtos_interno/$1',
    'pre-fabricados/obras' => 'frontend/fabricados_obras',
    'pre-fabricados/obras/(:any)' => 'frontend/fabricados_obras_interno/$1',


    'engenharia' => 'frontend/engenharia',
    'engenharia/sobre' => 'frontend/engenharia_sobre',
    'engenharia/obras' => 'frontend/engenharia_obras',
    'engenharia/obras/(:any)' => 'frontend/engenharia_obras_interno/$1',

    'obras' => 'frontend/obras',
    'obras/(:any)' => 'frontend/obras_interno/$1',

    'noticias/(:any)' => 'frontend/noticia/$1',
    'noticias' => 'frontend/noticias',

    'responsabilidade' => 'frontend/responsabilidade',

	'login' => 'frontend/auth/login',
	'logout' => 'frontend/auth/logout',
	'area-restrita' => 'frontend/auth/area_restrita',
	'esqueci-minha-senha' => 'frontend/auth/recuperar_senha',
	'esqueci-minha-senha/(:hash)' => 'frontend/auth/alterar_senha/$1',

    '(:any)' => 'frontend/$1',
);
