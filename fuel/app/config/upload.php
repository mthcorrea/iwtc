<?php

return array(
	'auto_process'	=> false,
	'ext_whitelist'	=> array('img', 'jpg', 'jpeg', 'gif', 'png', 'pdf', 'doc', 'xls', 'ppt', 'pps', 'docx', 'xlsx', 'pptx', 'ppsx', 'epub'),
	'ext_blacklist'	=> array('sh', 'php', 'js', 'css', 'html'),
	'path'			=> DOCROOT.'assets'.DS.'upload'.DS,
	'create_path'	=> true,
	'path_chmod'	=> 0755,
	'file_chmod'	=> 0644,
	'auto_rename'	=> true,
	'overwrite'		=> false,
	'randomize'		=> false,
	'normalize'		=> true,
	'change_case'	=> 'lower',
	'max_length'	=> 0
);


