<?php
return array(
  'version' => 
  array(
    'app' => 
    array(
      'default' => 
      array(
        0 => '001_create_usuario',
        1 => '002_create_banner',
        2 => '003_create_configuracao',
        3 => '004_create_contato',
        4 => '005_create_postagem',
        5 => '006_create_obra',
        6 => '007_create_produto',
        7 => '008_create_vaga',
        8 => '009_create_foto',
        9 => '010_create_arquivo',
        10 => '011_add_logo_to_banner',
        11 => '012_add_numeros_to_configuracao',
      ),
    ),
    'module' => 
    array(
    ),
    'package' => 
    array(
    ),
  ),
  'folder' => 'migrations/',
  'table' => 'migration',
);
