<?php
return array(
	'base_url'  => null,

	/**
	 * url_suffix - Any suffix that needs to be added to
	 * URL's generated by Fuel. If the suffix is an extension,
	 * make sure to include the dot
	 *
	 *     'url_suffix' => '.html',
	 *
	 * Set this to an empty string if no suffix is used
	 */
	'url_suffix'  => '',

	/**
	 * index_file - The name of the main bootstrap file.
	 *
	 * Set this to false or remove if you using mod_rewrite.
	 */
	'index_file'  => false,

	/**
	 * auto enable profiling when in development
	 */
	'profiling'  => (\Fuel::$env == \Fuel::DEVELOPMENT),

	/**
	 * Settings for Cache class
	 */
	'caching'         => (\Fuel::$env == \Fuel::PRODUCTION),
	'cache_dir'       => APPPATH.'cache/',
	'cache_lifetime'  => 3600, // In Seconds

	/**
	 * Callback to use with ob_start(), set this to 'ob_gzhandler' for gzip encoding of output
	 */
	'ob_callback'  => 'ob_gzhandler',

	'errors'  => array(
		// Which errors should we show, but continue execution?
		'continue_on'  => array(),
		// How many errors should we show before we stop showing them? (prevents out-of-memory errors)
		'throttle'     => 10,
		// Should notices from Error::notice() be shown?
		'notices'      => true,
	),

    'language'           => 'pt_br',
    'language_fallback'  => 'en',
    'locale'             => 'pt_BR',
    'locales'            => array(
        'pt_br' => 'pt_BR',
        'en' => 'en_US',
        'es' => 'es_ES'
    ),
	'encoding' 			 => 'UTF-8',

	'server_gmt_offset'  => -3,
	'default_timezone'   => 'America/Sao_Paulo',

	/**
	 * Logging Threshold.  Can be set to any of the following:
	 *
	 * Fuel::L_NONE
	 * Fuel::L_ERROR
	 * Fuel::L_WARNING
	 * Fuel::L_DEBUG
	 * Fuel::L_INFO
	 * Fuel::L_ALL
	 */
	'log_threshold'    => Fuel::$env == Fuel::DEVELOPMENT ? Fuel::L_ALL : Fuel::L_ERROR,
	'log_path'         => APPPATH.'..'.DS.'logs/',
	'log_date_format'  => 'Y-m-d H:i:s',

	/**
	 * Security settings
	 */
	'security' => array(
		'csrf_autoload'    => false,
		'csrf_token_key'   => 'fuel_csrf_token',
		'csrf_expiration'  => 0,
		'uri_filter'       => array('htmlentities'),

		/**
		 * This input filter can be any normal PHP function as well as 'xss_clean'
		 *
		 * WARNING: Using xss_clean will cause a performance hit.  How much is
		 * dependant on how much input data there is.
		 */
		'input_filter'  => array(),

		/**
		 * This output filter can be any normal PHP function as well as 'xss_clean'
		 *
		 * WARNING: Using xss_clean will cause a performance hit.  How much is
		 * dependant on how much input data there is.
		 */
		'output_filter'  => array('Security::htmlentities'),

		/**
		 * Whether to automatically filter view data
		 */
		'auto_filter_output'  => true,

		/**
		 * With output encoding switched on all objects passed will be converted to strings or
		 * throw exceptions unless they are instances of the classes in this array.
		 */
		'whitelisted_classes' => array(
			'Fuel\\Core\\Response',
			'Fuel\\Core\\View',
			'Fuel\\Core\\ViewModel',
			'Closure',
		)
	),

	/**
	 * Cookie settings
	 */
	'cookie' => array(
		// Number of seconds before the cookie expires
		'expiration'  => 0,
		// Restrict the path that the cookie is available to
		'path'        => '/',
		// Restrict the domain that the cookie is available to
		'domain'      => null,
		// Only transmit cookies over secure connections
		'secure'      => false,
		// Only transmit cookies over HTTP, disabling Javascript access
		'http_only'   => false,
	),

	/**
	 * To enable you to split up your application into modules which can be
	 * routed by the first uri segment you have to define their basepaths
	 * here. By default empty, but to use them you can add something
	 * like this:
	 *      array(APPPATH.'modules'.DS)
	 */
	'module_paths' => array(
		APPPATH.'modules'.DS
	),

	'package_paths' => array(
		PKGPATH
	),


	/**************************************************************************/
	/* Always Load                                                            */
	/**************************************************************************/
	'always_load'  => array(

		/**
		 * These packages are loaded on Fuel's startup.  You can specify them in
		 * the following manner:
		 *
		 * array('auth'); // This will assume the packages are in PKGPATH
		 *
		 * // Use this format to specify the path to the package explicitly
		 * array(
		 *     array('auth'	=> PKGPATH.'auth/')
		 * );
		 */
		'packages'  => array(
			'orm', 'auth', 'casset', 'message', 'email', 'special_agent', 'cart',
		),

		/**
		 * These modules are always loaded on Fuel's startup. You can specify them
		 * in the following manner:
		 *
		 * array('module_name');
		 *
		 * A path must be set in module_paths for this to work.
		 */
		'modules'  => array(
			'configuracao', 'usuario', 'contato',
		),

		/**
		 * Classes to autoload & initialize even when not used
		 */
		'classes'  => array(
			'funcoes', 'breadcrumb_controle'
		),

		/**
		 * Configs to autoload
		 *
		 * Examples: if you want to load 'session' config into a group 'session' you only have to
		 * add 'session'. If you want to add it to another group (example: 'auth') you have to
		 * add it like 'session' => 'auth'.
		 * If you don't want the config in a group use null as groupname.
		 */
		'config'  => array(
			'site', 'master', 'upload',
		),

		/**
		 * Language files to autoload
		 *
		 * Examples: if you want to load 'validation' lang into a group 'validation' you only have to
		 * add 'validation'. If you want to add it to another group (example: 'forms') you have to
		 * add it like 'validation' => 'forms'.
		 * If you don't want the lang in a group use null as groupname.
		 */
		'language'  => array('message'),
	),

);

/* End of file config.php */
