<?php

return array(
    'controle' => array(
        'dicionario' => array(
            'painel' => array(
                'label' => 'Painel',
                'link' => 'inicial',
            ),
            'usuario' => array(
                'label' => 'Usuários',
                'link' => 'usuario/listar',
            ),
            'listar' => array(
                'label' => 'Listar',
            ),
            'adicionar' => array(
                'label' => 'Adicionar',
            ),
            'editar' => array(
                'label' => 'Editar',
            ),
            'visualizar' => array(
                'label' => 'Visualizar',
            ),
            'configuracoes' => array(
                'label' => 'Configurações',
            ),
            'atribuir' => array(
                'label' => 'Atribuir Categoria',
            ),
            'upload_rapido' => array(
                'label' => 'Upload Rápido',
            ),
            'produto' => array(
                'label' => 'Produtos',
            ),
            'aprovado' => array(
                'label' => 'Aprovados'
            ),
            'inicial' => array(
                'label' => 'Inicial'
            ),
            'minhaconta' => array(
                'label' => 'Minha Conta'
            ),
            'porcao' => array(
                'label' => 'Porções'
            ),
            'institucional' => array(
                'label' => 'Conteúdos Institucionais'
            ),
            'postagem' => array(
                'label' => 'Postagens',
                'link' => 'postagem/listar',
            ),
            'categoria' => array(
                'label' => 'Categorias',
				'link' => 'obra/categoria/listar',
            ),
			'subcategoria' => array(
				'label' => 'Subcategorias',
				'link' => 'obra/subcategoria/listar',
			),
            'noticia' => array(
                'label' => 'Notícia'
            ),
            'pagina' => array(
                'label' => 'Página',
                'link' => 'pagina/listar',
            ),
            'banner' => array(
                'label' => 'Banner',
                'link' => 'banner/listar',
            ),
            'contato' => array(
                'label' => 'Contato',
                'link' => 'contato/listar',
            ),
			'obra' => array(
				'label' => 'Obras',
				'link' => 'obra/listar',
			),
			'vaga' => array(
				'label' => 'Vagas',
				'link' => 'vaga/listar',
			),
			'foto' => array(
				'label' => 'Fotos',
				'link' => 'foto/listar',
			),
			'midia' => array(
				'label' => 'Mídias',
				'link' => 'midia/listar',
			),
			'arquivo' => array(
				'label' => 'Arquivos',
				'link' => 'arquivo/listar',
			),
            'atracao' => array(
                'label' => 'Atrações',
                'link' => 'atracao/listar',
            ),
            'representante' => array(
                'label' => 'Representantes',
                'link' => 'representante/listar',
            ),
            'cidade' => array(
                'label' => 'Cidades',
                'link' => 'representante/cidade/listar',
            ),
            'orcamento' => array(
                'label' => 'Orçamentos',
                'link' => 'orcamento/listar',
            ),
			'miniatura' => array(
				'label' => 'Miniaturas',
				'link' => 'pagina/miniatura/listar',
			),
			'configuracao' => array(
				'label' => 'Configurações',
				'link' => 'configuracao/editar/1',
			),
        ),
    ),
);
