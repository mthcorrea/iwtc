<?php

return array(
	'fuelphp' => array(
		'app_created' => function () {
			// After FuelPHP initialised

			/* Configurações */
			if (\DBUtil::table_exists(\Configuracao\Model_Configuracao::table())) {
				$config = \Configuracao\Model_Configuracao::find(1);
				$config_campos = \Configuracao\Model_Configuracao::get_properties();

				$config_geral = array();

				if ($config) {
					foreach ($config_campos as $campo) {
						$config_geral[$campo] = $config->{$campo};
					}

					\Config::set('site.geral', $config_geral);
				}
			}

		},
		'request_created' => function () {
			// After Request forged
		},
		'request_started' => function () {
			// Request is requested
		},
		'controller_started' => function () {
			// Before controllers before() method called
		},
		'controller_finished' => function () {
			// After controllers after() method called
		},
		'response_created' => function () {
			// After Response forged
		},
		'request_finished' => function () {
			// Request is complete and Response received
		},
		'shutdown' => function () {
			// Output has been send out
		},
	),
);
