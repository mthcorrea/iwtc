<?php
namespace Usuario;

class Controller_Controle_Login extends \Controller_Controle_Template
{
	public function action_index()
	{
		$data = array();
		\Auth::check() and \Response::redirect('/controle/inicial');

		if (\Input::method() == 'POST')
		{
			$val = Model_Usuario::forge()->validation('login');
			if ($val->run())
			{
				if (\Auth::login(\Input::post('username'), \Input::post('password')))
				{
					\Response::redirect('/controle/inicial');
				}
				else
				{
					\Message::error(__('message.login.erro'));
				}
			}
			else
			{
				\Message::error($val->error());
			}
		}
		$this->template->body_class = 'login bg-' . rand(0,1);
		$this->template->content = \View::forge(__NAMESPACE__.'::controle/login', $data);
	}
}