<?php
namespace Usuario;

class Controller_Controle_Recuperarsenha extends \Controller_Controle_Template
{
   public function action_index()
   {
      $view = __NAMESPACE__.'::controle/recuperarsenha/email';

      $data = array();

      // Logado? Redireciona.
      \Auth::check() and \Response::redirect('/');

      if (\Input::Method() == 'POST')
      {
         $val = Model_Usuario::forge()->validation('recuperarsenha');

         if ($val->run())
         {
            $registro = Model_Usuario::query()->and_where_open()
                                                ->where('username', \Input::post('username'))
                                                ->or_where('email', \Input::post('username'))
                                             ->and_where_close()
                                             ->where('status',1)
                                             ->get_one();

            if ($registro)
            {
               $recuperar_senha_hash = md5(uniqid(time()).$registro->id.$registro->username);
               $crypt_id = \Crypt::encode($registro->id);

               /* atualizo o banco com a nova hash */
               $registro->set(array('recuperar_senha_hash' => $recuperar_senha_hash));
               $registro->save();

               $data['registro'] = $registro;
               $data['crypt_id'] = $crypt_id;

               /* enviando o e-mail */
               \Package::load('email');

               $email = \Email::forge();
               $email->to($registro->email, $registro->nome);
               $email->subject('Assistência de senha');

               $conteudo = array(
                  'titulo' => 'Assistência de recuperação de senha',
                  'texto'  => \View::forge(__NAMESPACE__.'::email/recuperarsenha', $data)
               );

               $email->html_body(\View::forge('controle/layout/email', $conteudo));

               try
               {
                   $email->send();
                   $view = __NAMESPACE__.'::controle/recuperarsenha/enviado';
               }
               catch(\Exception $e)
               {
                   \Message::error(__('message.email.erro'));
               }
            }
            else
            {
               \Message::error(__('message.recuperarsenha.nao_encontrado'));
            }

         }
         else
         {
            \Message::error($val->error());
         }
      }

      $this->template->body_class = 'login';
      $this->template->title = 'Recuperar Senha';
      $this->template->content = \View::forge($view, $data);

   }

   public function action_alterarsenha($hash = null)
   {
       $data = array();
       $view = __NAMESPACE__.'::controle/recuperarsenha/alterarsenha';

       if (\Auth::check() or ! isset($hash))
       {
           \Response::redirect('/');
       }


       $recuperar_senha_hash = substr($hash,0,32);
       $id = \Crypt::decode(substr($hash,32, strlen($hash)));


       if (is_numeric($id) and $id > 0 and strlen($recuperar_senha_hash) == 32)
       {

           $registro = Model_Usuario::query()->where('id', $id)
                                             ->where('recuperar_senha_hash', $recuperar_senha_hash)
                                             ->where('status',1)
                                             ->get_one();

           if ($registro)
           {
               $data['registro'] = $registro;

               if (\Input::method() == 'POST')
               {
                   $val = $registro->validation('recuperarsenha_alterarsenha');

                   if ($val->run())
                   {
                       $auth = \Auth::instance();

                       $old_password = $auth->reset_password($registro->username);
                       if ($auth->change_password($old_password, \Input::post('password'), $registro->username))
                       {
                           \Message::success(__('message.recuperarsenha.sucesso'));
                           $view = __NAMESPACE__.'::controle/recuperarsenha/alterado';

                           try
                           {
                              $registro->set(array('recuperar_senha_hash' => ''));
                              $registro->save();
                           }
                           catch (\Exception $e)
                           {
                              /* Não exibe nada, pois o usuário já alterou a senha. */
                           }
                       }
                       else
                       {
                           \Message::error('message.recuperarsenha.erro');
                       }
                   }
                   else
                   {
                       \Message::error($val->error());
                   }
               }
           }
           else
           {
               $view = __NAMESPACE__.'::controle/recuperarsenha/invalido';
           }
       }
       else
       {
          $view = __NAMESPACE__.'::controle/recuperarsenha/invalido';
       }

       $this->template->body_class = 'login';
       $this->template->title = 'Recuperar Senha';
       $this->template->content = \View::forge($view, $data,false);
   }
}