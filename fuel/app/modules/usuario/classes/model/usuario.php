<?php
namespace Usuario;
class Model_Usuario extends \Orm\Model
{
    protected static $_properties = array(
        'id',
        'nome',
        'username',
        'password',
        'email',
        'group',
        'recuperar_senha_hash',
        'last_login',
        'previous_login',
        'login_hash',
        'profile_fields',
        'status',
        'user_id',
        'created_at',
        'updated_at',
    );

    protected static $_to_array_exclude = array(
        'password', 'login_hash'
    );

	protected static $_table_name = 'usuario';

	protected static $_status = array (
        1 => array('titulo' => 'Ativo', 'class' => 'success'),
        0 => array('titulo' => 'Inativo', 'class' => 'danger'),
    );

	protected static $_group = array (
		2 => 'Administrador',
		3 => 'Usuário Comum',
	);

	public static function listar($retorno = array(), $fk_group = null)
	{
		is_array($retorno) or $retorno = array();

		$usuarios = static::query();
		if (!is_null($fk_group)) {
			$usuarios->where('group', $fk_group);
		}
		$usuarios = $usuarios->order_by('nome', 'asc')->get();

		foreach ($usuarios as $usuario) {
			$retorno[$usuario->id] = $usuario->nome;
		}

		return $retorno;
	}

    public static function lista_status()
    {
        $status = array();

        foreach (static::$_status as $indice => $valor)
        {
            $status[$indice] = $valor['titulo'];
        }

        return $status;
    }

	public static function lista_group()
	{
		$status = array();

		foreach (static::$_group as $indice => $valor)
		{
			$status[$indice] = $valor;
		}

		return $status;
	}

    public function status($formato = 'titulo')
    {
        return static::$_status[$this->status][$formato];
    }

	public function group()
	{
		return static::$_group[$this->group];
	}

	public function enviar_recuperar_senha()
	{
		if ($this->status == 1) {
			$recuperar_senha_hash = md5(uniqid(time()) . $this->id . $this->username);
			$crypt_id = \Crypt::encode($this->id);
			$this->recuperar_senha_hash = $recuperar_senha_hash;
			$this->save();
			$data['registro'] = $this;
			$data['crypt_id'] = $crypt_id;

			$email = \Email::forge();
			$email->to($this->email, $this->nome);
			$email->subject('Recuperação de Senha');
			$conteudo = array(
				'titulo' => 'Assistência de recuperação de senha',
				'texto' => \View::forge('email/auth/recuperar_senha', $data)
			);
			$email->html_body(\View::forge('email/template', $conteudo));
			try {
				if (\Fuel::$env == \Fuel::DEVELOPMENT) {
					echo \View::forge('email/template', $conteudo);
					echo '<br >';
				} else {
					$email->send();
				}
				return true;
			} catch (\Exception $e) {
				return false;
			}
		}
		return false;
	}

	public function validation($pagina = '')
    {
    	$val = \Validation::forge();

    	switch ($pagina)
    	{
    		case 'login':
    			$val->add('username', 'E-mail ou Usuário')->add_rule('required');
           		$val->add('password', 'Senha')->add_rule('required');
    			break;

    		case 'recuperarsenha':
    			$val->add('email', 'E-mail')->add_rule('required');
    			break;

    		case 'recuperarsenha_alterarsenha':
    			$val->add('password','Nova senha')->add_rule('required');
                $val->add('password_again','Confirmar nova senha')->add_rule('match_field','password');
				break;

			case 'usuario':
				$val->add_field('group', 'Permissão', 'required|valid_string[numeric]|numeric_min[3]');
				break;

			default:
			    $val->add_field('nome', 'Nome', 'required|min_length[3]|max_length[100]');
	    		$val->add_field('email', 'E-mail', 'required|valid_email|unique[usuario,email,'.$this->id.']');
	    		$val->add_field('username', 'Usuário', 'required|min_length[3]|max_length[50]|unique[usuario,username,'.$this->id.']');
	    		$val->add_field('new_password', 'Nova senha', 'min_length[3]|max_length[100]');
           	    $val->add_field('new_password_again', 'Confirmar nova senha', 'match_field[new_password]');

                $val->add_field('status', 'Status', 'required|valid_string[numeric]');

	    		if (is_null($this->id))
	    		{
	    			$val->add_field('password', 'Senha', 'required|min_length[3]|max_length[100]');
	    			$val->add_field('password_again', 'Confirmar senha', 'required_with[password]|match_field[password]');
	    		}
	    		else
	    		{
	    			$val->add_field('password', 'Senha', 'min_length[3]|max_length[100]');
	    			$val->add_field('password_again', 'Confirmar senha', 'required_with[password]|match_field[password]');
	    		}
				break;
    	}

    	return $val;
    }

}
