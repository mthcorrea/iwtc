<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Filtrar <?php echo $titulo['plural']; ?></h3>
	</div>
	<div class="panel-body">
		<?php echo Form::open(array('class'=>'form-horizontal', 'method'=>'get', 'name'=>'search')); ?>
			<div class="form-group">
				<div class="col-sm-6">
					<?php echo Form::input('fast_query', Input::get('fast_query'), array('class'=>'form-control', 'placeholder'=>'Filtro')); ?>
					<p class="help-block">Use o campo acima para efetuar uma busca nos produtos. A busca é feita no nome, usuário e e-mail</p>
				</div>
				<div class="col-sm-3">
					<?php echo Form::select('fast_order', Input::get('fast_order'), array('Últimos Cadastrados', 'Ordem Alfabética'), array('class'=>'form-control')); ?>
				</div>
				<div class="col-sm-3">
					<button type="submit" class="btn btn-block btn-success"><span class="glyphicon glyphicon-search"></span> Filtrar <?php echo $titulo['plural']; ?></button>
				</div>
			</div>
		<?php echo Form::close(); ?>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">
		<span class="check_box_all pull-left"><input type="checkbox" class="check_all" /></span>
		<h3 class="panel-title"><?php echo Pagination::get('total_items'); ?> <?php echo $titulo['plural']; ?> - <small>Página <?php echo Pagination::get('current_page'); ?> de <?php echo Pagination::get('total_pages'); ?></small></h3>
		<div class="btns">
            <?php if (\Auth::has_access('cliente.adicionar')): ?>
			<a href="<?php echo \Uri::controller('adicionar'); ?>" class="btn btn-success">
				<span class="glyphicon glyphicon-plus-sign"></span> Criar <?php echo $titulo['singular']; ?>
			</a>
			<?php endif; ?>
		</div>
	</div>
	<div class="panel-body">
		<table class="table table-hover">
			<tbody class="rowlink" data-link="row">
				<?php
					if (isset($registros) and $registros)
					{
						foreach ($registros as $registro)
						{
				?>
				<tr data-id="<?php echo $registro->id; ?>">
					<td class="check_box rowlink-skip">
						<input type="checkbox" name="registros[]" class="td_checkbox" value="<?php echo $registro->id; ?>" />
					</td>
					<td width="50%">
						<a href="<?php echo \Uri::controller('/editar/'.$registro->id); ?>" title="Editar este item" class="titulo">
							<?php echo $registro->nome; ?><br />
							<small>
								Usuário: <?php echo $registro->username; ?><br />
								E-mail: <?php echo $registro->email; ?>
							</small>
						</a>
					</td>
					<td>
						<?php echo Auth::group()->get_name($registro->group); ?>
					</td>
					<td class="status">
						<span class="label label-<?php echo $registro->status('class'); ?>">
			                <?php echo $registro->status(); ?>
			             </span>
					</td>
				</tr>
				<?php
						}
					}
					else
					{
				?>
						<tr>
				            <td colspan="100%">
				                <p style="text-align:center;">Nenhum item foi encontrado.</p>
				            </td>
				        </tr>
				<?php
					}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php echo \View::forge('controle/layout/paginacao'); ?>