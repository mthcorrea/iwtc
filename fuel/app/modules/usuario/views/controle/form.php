<?php echo \Form::open(array("class" => "form-horizontal")); ?>
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Dados Básicos</h3>
			<div class="btns">
				<?php if (\Auth::has_access('cliente.adicionar') and \Auth::get('id') != $registro->id): ?>
					<a href="<?php echo \Uri::controller('adicionar'); ?>" class="btn btn-success">
						<span class="glyphicon glyphicon-plus-sign"></span> Criar novo
					</a>
				<?php endif; ?>
			</div>
		</div>
		<div class="panel-body">
			<div class="form-group bottom">
				<?php echo \Form::label('* Grupo', 'group', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-2">
					<?php
					echo \Form::select('group', \Input::post('group', $registro->group), $registro::lista_group(), array('class' => 'form-control'));
					?>
				</div>
			</div>
			<div class="form-group">
				<?php echo \Form::label('* Nome', 'nome', array('class' => 'col-sm-3 control-label')); ?>
				<div class="col-sm-5">
					<?php echo \Form::input('nome', Input::post('nome', $registro->nome), array('class' => 'form-control', 'required')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo \Form::label('* E-mail', 'email', array('class' => 'col-sm-3 control-label')); ?>
				<div class="col-sm-4">
					<?php echo \Form::input('email', Input::post('email', $registro->email), array('class' => 'form-control', 'required')); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Dados de Acesso ao Sistema</h3>
		</div>
		<div class="panel-body">
			<div class="form-group">
				<?php echo \Form::label('* Usuário', 'username', array('class' => 'col-sm-3 control-label')); ?>
				<div class="col-sm-3">
					<?php
					if (is_null($registro->id)) {
						echo \Form::input('username', Input::post('username', $registro->username), array('class' => 'form-control', 'required'));
					} else {
						echo \Form::input('username', Input::post('username', $registro->username), array('class' => 'form-control', 'type' => 'hidden'));
						echo html_tag('p', array('class' => 'form-control-static'), $registro->username);
					}
					?>
				</div>
			</div>
			<?php
			if (!$registro->id) {
				?>
				<div class="form-group">
					<?php echo \Form::label('* Senha', 'password', array('class' => 'col-sm-3 control-label')); ?>
					<div class="col-sm-3">
						<?php echo \Form::password('password', '', array('class' => 'form-control', 'required')); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo \Form::label('Confirmar senha', 'password_again', array('class' => 'col-sm-3 control-label')); ?>
					<div class="col-sm-3">
						<?php echo \Form::password('password_again', '', array('class' => 'form-control', 'required')); ?>
					</div>
				</div>
				<?php
			} else {
				?>
				<div class="form-group">
					<?php echo \Form::label('Nova senha', 'password', array('class' => 'col-sm-3 control-label')); ?>
					<div class="col-sm-3">
						<?php echo \Form::password('password', '', array('class' => 'form-control')); ?>
					</div>
				</div>
				<div class="form-group">
					<?php echo \Form::label('Confirmar nova senha', 'password_again', array('class' => 'col-sm-3 control-label')); ?>
					<div class="col-sm-3">
						<?php echo \Form::password('password_again', '', array('class' => 'form-control')); ?>
					</div>
				</div>
				<?php
			}
			?>
			<hr/>
			<?php
			if ($registro->id !== $current_user->id) {
				?>

				<div class="form-group bottom">
					<?php echo \Form::label('* Status', 'status', array('class' => 'col-sm-3 control-label')); ?>
					<div class="col-sm-2">
						<?php
						echo \Form::select('status', \Input::post('status', $registro->status), $registro::lista_status(), array('class' => 'form-control'));
						?>
					</div>
				</div>
				<?php
			}
			?>
		</div>
	</div>

	<div class="btn-save-container">
		<div class="btn-back pull-left">
			<?php
			echo \Html::anchor(\Uri::back(), '<span class="glyphicon glyphicon-chevron-left"></span> Voltar', array('class' => 'btn btn-primary'));
			?>
		</div>
		<div class="btn-save pull-right">
			<button type="submit" class="btn btn-lg btn-success"><span class="glyphicon glyphicon-ok"></span>
				Salvar <?php echo $titulo['singular']; ?></button>
		</div>
		<div class="clearfix"></div>
	</div>
<?php echo \Form::close(); ?>
