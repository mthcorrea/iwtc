<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="page-header">
				<h2>Recuperar Senha</h2>
			</div>
			<p>O URL que você tentou usar está incorreto ou não é mais válido.<br /><br />Se você quiser tentar novamente, inicie o processo de recuperação desde o início.</p>
		</div>
	</div>
</div>