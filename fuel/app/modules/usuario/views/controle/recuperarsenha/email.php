<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?php echo Form::open(array('class'=>'form')); ?>
				<div class="page-header">
					<h2>Recuperar Senha</h2>
				</div>
				<span class="help-block">Para redefinir sua senha, digite o e-mail ou nome de usuário que você usa para fazer login.</span>
				<div class="form-group">
					<?php
						echo Form::input('username',Input::post('username'),array('class'=>'form-control','placeholder'=>'Usuário ou e-mail','autofocus'));
					?>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-8">
						<button class="btn btn-primary btn-block" type="submit">OK!</button>
					</div>
				</div>
			<?php echo Form::close(); ?>
		</div>
	</div>
</div>