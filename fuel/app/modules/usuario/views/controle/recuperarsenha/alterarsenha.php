<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?php echo Form::open(array('class'=>'form')); ?>
				<div class="page-header">
					<h2>Recuperar Senha</h2>
				</div>
				<span class="help-block">Digite uma nova senha para <?php echo $registro->email; ?>.<br /><br /><small>Recomendamos que você crie uma senha exclusiva, que você não use para nenhum outro site.</small></span>
				<div class="form-group">
					<?php
						echo Form::password('password','',array('class'=>'form-control','placeholder'=>'Nova senha'));
					?>
				</div>
				<div class="form-group">
					<?php
						echo Form::password('password_again','',array('class'=>'form-control','placeholder'=>'Confirmar nova senha'));
					?>
				</div>
				<div class="row">
					<div class="col-md-4 col-md-offset-8">
						<button class="btn btn-primary btn-block" type="submit">Redefinir Senha</button>
					</div>
				</div>
			<?php echo Form::close(); ?>
		</div>
	</div>
</div>