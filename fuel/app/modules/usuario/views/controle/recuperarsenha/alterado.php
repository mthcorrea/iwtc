<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="page-header">
				<h2>Recuperar Senha</h2>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-6">
					<?php echo \Html::anchor('controle/login','Clique aqui e faça o login.', array('class' => 'btn btn-block btn-primary')); ?>
				</div>
			</div>
		</div>
	</div>
</div>