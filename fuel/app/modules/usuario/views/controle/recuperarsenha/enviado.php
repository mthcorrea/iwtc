<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="page-header">
				<h2>Recuperar Senha</h2>
			</div>
			<p><strong>E-mail enviado!</strong></p>
			<p>Para recuperar a senha, siga as instruções que enviamos para seu endereço de e-mail <?php echo $registro->email; ?></p>
			<p><small>Não recebeu o e-mail para redefinição de senha? Em sua pasta de spam, procure um e-mail "Assistência de senha". Se ainda assim não conseguir encontrá-lo, <?php echo \Html::anchor('controle/recuperarsenha','tente novamente.'); ?></small></p>
		</div>
	</div>
</div>