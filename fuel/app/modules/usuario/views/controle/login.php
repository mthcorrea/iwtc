<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<?php echo Form::open(array('class'=>'form')); ?>
				<div class="page-header">
					<h2>Acesso ao Sistema</h2>
				</div>
				<div class="form-group">
					<?php
						echo Form::input('username',Input::post('username'),array('class'=>'form-control','placeholder'=>'Usuário ou e-mail','autofocus'));
					?>
				</div>
				<div class="form-group">
					<?php
						echo Form::password('password','',array('class'=>'form-control','placeholder'=>'senha'));
					?>
				</div>
				<div class="row">
					<div class="col-md-8">
						<span class="help-block"><?php echo Html::anchor('controle/recuperarsenha','Esqueceu sua senha?'); ?></span>
					</div>
					<div class="col-md-4">
						<button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-log-in"></span> Entrar</button>
					</div>
				</div>
			<?php echo Form::close(); ?>
		</div>
	</div>
</div>