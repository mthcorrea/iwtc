			<?php
				if (isset($registros) and $registros) {
					foreach ($registros as $registro) {
			?>
			<tr data-id="<?php echo $registro->id; ?>">
				<?php
					if ( ! isset($painel) || $painel == false) {
				?>
				<td class="check_box rowlink-skip">
					<input type="checkbox" name="registros[]" class="td_checkbox" value="<?php echo $registro->id; ?>" />
				</td>
				<?php
					}
				?>
				<td width="50%">
					<a href="<?php echo \Uri::controller('/editar/'.$registro->id); ?>" title="Editar este item" class="titulo">
						<?php echo $registro->nome; ?><br />
						<small>
							Usuário: <?php echo $registro->username; ?><br />
							E-mail: <?php echo $registro->email; ?>
						</small>
					</a>
				</td>
				<td>
					<?php echo Auth::group()->get_name($registro->group); ?>
				</td>
				<td class="status">
					<span class="label label-<?php echo $registro->status('class'); ?>">
		                <?php echo $registro->status(); ?>
		             </span>
				</td>
			</tr>
			<?php
					}
				}
			?>