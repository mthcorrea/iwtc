<p>Olá, <strong><?php echo $registro->nome; ?></strong><br />
Para iniciar o processo de redefinição de senha <?php echo $registro->email; ?>, clique no link abaixo:</p>
<?php echo \Html::anchor(\Uri::create('controle/recuperarsenha/'.$registro->recuperar_senha_hash.$crypt_id), \Uri::create('controle/recuperarsenha/'.$registro->recuperar_senha_hash.$crypt_id)); ?>
</br>
<p>Se o link acima não funcionar, copie e cole o URL em uma nova janela do navegador.</p>
<p>Caso você tenha recebido este e-mail por engano, é provável que outro usuário tenha inserido seu endereço de e-mail inadvertidamente ao tentar
redefinir uma senha. Se você não iniciou a solicitação, não precisa realizar qualquer ação adicional, podendo desconsiderar este e-mail com segurança.</p>