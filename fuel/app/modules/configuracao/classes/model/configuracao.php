<?php

namespace Configuracao;

class Model_Configuracao extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'titulo',
		'telefone',
		'celular',
		'email',
		'endereco',

		'facebook',
		'google_plus',
		'instagram',
		'linkedin',
		'pinterest',
		'youtube',
		'twitter',

		'meta_title',
		'meta_description',
		'meta_keywords',

		'conteudo_extra_css',
		'conteudo_extra_texto',
	);

	protected static $_observers = array(
		'\Observer_Controle' => array(
			'events' => array('before_delete'),
		),
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'configuracao';

	public static function get_properties()
	{
		return static::$_properties;
	}

	public function validation()
	{
		$val = \Validation::forge();

		$val->add_field('titulo', 'Título', 'required|max_length[150]');
		$val->add_field('telefone', 'Telefone', 'max_length[15]');
		$val->add_field('celular', 'Celular', 'max_length[15]');
		$val->add_field('email', 'E-mail', 'valid_email|max_length[200]');
		$val->add_field('endereco', 'Endereço', 'max_length[255]');

		$val->add_field('facebook', 'Facebook', 'max_length[255]');
		$val->add_field('google_plus', 'Google Plus', 'max_length[255]');
		$val->add_field('instagram', 'Instragarm', 'max_length[255]');
		$val->add_field('linkedin', 'Linked-In', 'max_length[255]');
		$val->add_field('pinterest', 'Pinterest', 'max_length[255]');
		$val->add_field('twitter', 'Twitter', 'max_length[255]');
		$val->add_field('youtube', 'Youtube', 'max_length[255]');

		$val->add_field('sendi_pre_num_1', 'Sendi Pré Núm. 1', 'valid_string[numeric]');
		$val->add_field('sendi_pre_num_2', 'Sendi Pré Núm. 2', 'valid_string[numeric]');
		$val->add_field('sendi_pre_num_3', 'Sendi Pré Núm. 3', 'valid_string[numeric]');
		$val->add_field('sendi_pre_num_4', 'Sendi Pré Núm. 4', 'valid_string[numeric]');

		$val->add_field('sendi_engenharia_num_1', 'Sendi Engenharia Núm. 1', 'valid_string[numeric]');
		$val->add_field('sendi_engenharia_num_2', 'Sendi Engenharia Núm. 2', 'valid_string[numeric]');
		$val->add_field('sendi_engenharia_num_3', 'Sendi Engenharia Núm. 3', 'valid_string[numeric]');
		$val->add_field('sendi_engenharia_num_4', 'Sendi Engenharia Núm. 4', 'valid_string[numeric]');

		$val->add_field('meta_title', 'Meta Title', 'max_length[120]');
		$val->add_field('meta_description', 'Meta Description', 'max_length[155]');
		$val->add_field('meta_keywords', 'Meta Keywords', 'max_length[155]');

		return $val;
	}
}
