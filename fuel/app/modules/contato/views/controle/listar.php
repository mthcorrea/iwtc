<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Filtrar <?php echo $titulo['plural']; ?></h3>
    </div>
    <div class="panel-body">
        <?php echo Form::open(array('class' => 'form-horizontal', 'method' => 'get', 'name' => 'search')); ?>
        <div class="form-group">
            <div class="col-sm-6">
                <?php echo Form::input('fast_query', Input::get('fast_query'), array('class' => 'form-control', 'placeholder' => 'Filtro')); ?>
                <p class="help-block">Use o campo acima para efetuar uma busca. A busca é feita apenas no título.</p>
            </div>
            <div class="col-sm-3">
                <?php echo Form::select('fast_tipo', Input::get('fast_tipo'), \Contato\Model_Contato::lista_tipos(), array('class'=>'form-control')); ?>
            </div>
            <div class="col-sm-3">
                <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-search"></span>
                    Filtrar <?php echo $titulo['plural']; ?></button>
            </div>
        </div>
        <?php echo Form::close(); ?>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo \Pagination::get('total_items'); ?> <?php echo \Inflector::pluralize($titulo['singular'], \Pagination::get('total_items')); ?> - <small>Exibindo <span class="por_pagina"><?php echo count($registros); ?></span> de <span class="total_itens"><?php echo \Pagination::get('total_items'); ?></span></small></h3>
    </div>
    <div class="panel-body listar">
        <table class="table table-striped table-hover table-fixed-header">
            <?php
            echo \TableSort::create_table_head();
            ?>
            <tbody class="rowlink" data-link="row">
            <?php if (isset($registros) and $registros) { ?>
                <?php echo render('controle/listar_registros', array('registros' => $registros)); ?>
            <?php } else { ?>
                <tr>
                    <td colspan="100%">
                        <p style="text-align:center;">Nenhum item foi encontrado.</p>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<?php echo View::forge('controle/layout/paginacao'); ?>
