<?php echo Form::open(array("class" => "form-horizontal", 'enctype' => 'multipart/form-data')); ?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Dados Básicos</h3>
	</div>
	<div class="panel-body">
		<?php if ($registro->fk_departamento) { ?>
			<div class="form-group">
				<?php echo Form::label('Departamento', 'departamento', array('class' => 'col-sm-4 control-label')); ?>

				<div class="col-sm-6">
					<div class="form-control-show">
						<?php echo $registro->departamento(); ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<?php if ($registro->fk_vaga) { ?>
			<div class="form-group">
				<?php echo Form::label('Vaga', 'vaga', array('class' => 'col-sm-4 control-label')); ?>

				<div class="col-sm-6">
					<div class="form-control-show">
						<?php echo $registro->vaga(); ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="form-group">
			<?php echo Form::label('Tipo', 'tipo', array('class' => 'col-sm-4 control-label')); ?>

			<div class="col-sm-6">
				<div class="form-control-show">
					<?php echo $registro->tipo(); ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('Nome', 'nome', array('class' => 'col-sm-4 control-label')); ?>

			<div class="col-sm-6">
				<div class="form-control-show">
					<?php echo $registro->nome; ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<?php echo Form::label('E-mail', 'email', array('class' => 'col-sm-4 control-label')); ?>

			<div class="col-sm-6">
				<div class="form-control-show">
					<?php echo $registro->email; ?>
				</div>
			</div>
		</div>

		<?php if ($registro->assunto) { ?>
			<div class="form-group">
				<?php echo Form::label('Assunto', 'assunto', array('class' => 'col-sm-4 control-label')); ?>

				<div class="col-sm-6">
					<div class="form-control-show">
						<?php echo $registro->assunto; ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="form-group">
			<?php echo Form::label('Mensagem', 'mensagem', array('class' => 'col-sm-4 control-label')); ?>

			<div class="col-sm-6">
				<div class="form-control-show">
					<?php echo $registro->mensagem; ?>
				</div>
			</div>
		</div>

		<?php if ($registro->arquivo) { ?>
			<div class="form-group">
				<?php echo Form::label('Arquivo', 'arquivo', array('class' => 'col-sm-4 control-label')); ?>

				<div class="col-sm-6">
					<div class="form-control-show">
						<?php echo $registro->arquivo(); ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<div class="form-group">
			<?php echo Form::label('Data', 'data', array('class' => 'col-sm-4 control-label')); ?>

			<div class="col-sm-6">
				<div class="form-control-show">
					<?php echo $registro->created_at('br_full'); ?>
				</div>
			</div>
		</div>

	</div>
</div>

<div class="btn-save-container">
	<div class="btn-back pull-left">
		<?php echo Html::anchor(Uri::back(), '<span class="glyphicon glyphicon-chevron-left"></span> Voltar', array('class' => 'btn btn-primary')); ?>
	</div>
	<div class="clearfix"></div>
</div>

<?php echo Form::close(); ?>

<?php
$js_inline = "";
Casset::js_inline($js_inline);
?>
