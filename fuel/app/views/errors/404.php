<section class="content" id="main-content">
	<div class="page">
		<header class="page-header">
			<div class="container">
				<h1>Página não encontrada :(</h1>
			</div>
		</header>
		<section class="page-content">
			<div class="container">
				<div class="row">
					<div class="col-sm-7">
						<p>Tem alguma dúvida, sugestão ou crítica a fazer? Então entre em contato com a gente. Sua opinião é fundamental para o nosso aperfeiçoamento.</p>
					</div>
					<div class="col-sm-5">
					</div>
				</div>
			</div>
		</section>
	</div>
</section>

