<div class="row">
    <?php
    $pages_span = 8;
    if (\Request::action_exists('aplicar'))
    {
        $pages_span = 4;
    ?>
        <div class="col-md-4">
            <form class="form-inline" name="list-actions" method="post" action="<?php echo \Uri::controller('/aplicar/'); ?>"  onSubmit="load_selected_itens('.td_checkbox'); return true;">
                <input type="hidden" class="td_checked" name="selected_itens" />
                <div class="form-group">
                    <select name="action" class="form-control">
                        <option value="0">Ações em Massa</option>
                        <?php
                        if (\Request::action_exists('ativar') )
                        {
                            echo '<option value="ativar">Ativar</option>';
                        }

                        if (\Request::action_exists('inativar'))
                        {
                            echo '<option value="inativar">Inativar</option>';
                        }

                        if (\Request::action_exists('excluir'))
                        {
                            echo '<option value="excluir">Excluir</option>';
                        }
                        ?>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">Aplicar</button>
            </form>
        </div>
    <?php
    }
    ?>
    <div class="col-md-4 col-md-offset-<?php echo $pages_span; ?>">
        <div class="pages text-right">
            <?php 
            if ($pagination = \Pagination::create_links())
            {
                echo $pagination;
            } 
            else
            {

                echo \Clickpagination::create_button();
            }

            ?>
        </div>
    </div>
</div>
<br />