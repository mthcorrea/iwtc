<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title">Texto (Conteúdo)</h3>
	</div>
	<div class="panel-body">
		<div class="form-group">
			<div class="col-sm-12">
				<?php echo Form::textarea('texto', Input::post('texto', $texto ), array('class' => 'form-control ckeditor')); ?>
			</div>
		</div>
	</div>
</div>
