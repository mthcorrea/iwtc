<?php echo Form::open(array("class"=>"form-horizontal",'enctype'=>'multipart/form-data')); ?>
	<div class="form-actions">
        <button type="submit" class="btn btn-success">Salvar Alterações</button>
        <button type="button" class="btn">Cancelar</button>
    </div>

	<fieldset>
		<legend>Dados Básicos</legend>

		<div class="control-group">
			<?php echo Form::label('* Titulo da Página', 'titulo', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::input('titulo', Input::post('titulo', $registro->titulo ), array('class' => 'span4')); ?>
				<span class="label show-tooltip" data-original-title="Este será o nome com que esta página aparecerá no site.">?</span>
			</div>
		</div>

		<div class="control-group">
			<?php echo Form::label('Texto', 'texto', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::textarea('texto', Input::post('texto', $registro->texto ), array('class' => 'span4 ckeditor')); ?>
				<span class="label show-tooltip" data-original-title="Conteúdo da página.">?</span>
			</div>
		</div>

		<div class="control-group">
			<?php echo Form::label('* Destinatários', 'destinatario', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::input('destinatario', Input::post('destinatario', $registro->destinatario ), array('class' => 'span4')); ?>
				<span class="label show-tooltip" data-original-title="Faça a separação dos destinatários com vírgula, exemplo: email@email.com, seuemail@seuemail.com">?</span>
			</div>
		</div>

	</fieldset>

	<fieldset>
		<legend>Configurações de SEO e OpenGraph do Facebook</legend>

		<div class="control-group">
			<?php echo Form::label('Título', 'meta_title', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::input('meta_title', Input::post('meta_title', $registro->meta_title ), array('class' => 'span4')); ?>
				<span class="label show-tooltip" data-original-title="Este será o título que aparecerá quando compartilharem esta página no facebook.">?</span>
			</div>
		</div>
		<div class="control-group">
			<?php echo Form::label('Descrição', 'meta_description', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::textarea('meta_description', Input::post('meta_description', $registro->meta_description ), array('class' => 'span4')); ?>
				<span class="label show-tooltip" data-original-title="Esta será a descrição que aparecerá no Google, ou quando compartilharem esta página no facebook.">?</span>
			</div>
		</div>
		<div class="control-group">
			<?php echo Form::label('Palavras Chaves', 'meta_keywords', array('class'=>'control-label')); ?>

			<div class="controls">
				<?php echo Form::input('meta_keywords', Input::post('meta_keywords', $registro->meta_keywords ), array('class' => 'span4')); ?>
				<span class="label show-tooltip" data-original-title="Palavras-chave ajudam os mecanismos de busca a identificarem o site.">?</span>
                <span class="help-block"><small>Use vírgula para separar as palavras-chave.</small></span>
			</div>
		</div>
	</fieldset>


	<div class="form-actions">
            <button type="submit" class="btn btn-success">Salvar Alterações</button>
            <button class="btn">Cancelar</button>
    </div>
<?php echo Form::close(); ?>