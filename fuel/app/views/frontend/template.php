<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="pt-br"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="pt-br"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="pt-br"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="pt-br">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
	<meta name="description" content="<?php echo $title; ?>">

	<title><?php echo $title; ?></title>

	<link id="page_favicon" href="<?php echo \Uri::create('assets/img/favicon.png'); ?>" rel="icon" type="image/x-icon"/>

	<?php
	if (isset($seo) and is_array($seo)) {
		foreach ($seo as $key => $value) {
			if ($key == 'canonical') {
				echo '<link rel="canonical" href="' . $value . '" />';
			} else {
				$type = (strpos($key, ':') === false) ? 'name' : 'property';
				echo \Html::meta($key, $value, $type);
			}
		}
	}
	?>

	<script type="text/javascript">
		const BASE_URL = '<?php echo \Uri::create('/'); ?>';
	</script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo \Uri::create(\Funcoes::auto_version('/assets/css/main.min.css')); ?>">
	<style type="text/css" rel="stylesheet">
		<?php if(isset($conteudo_extra_css) and $conteudo_extra_css){ ?>
		<?php echo $conteudo_extra_css ?>
		<?php } ?>
	</style>

	<?php echo Asset::js('jquery.min.js'); ?>
</head>

<body>

<?php echo $conteudo; ?>

<script src="<?php echo \Uri::create(\Funcoes::auto_version('/assets/js/main.min.js')); ?>"></script>
<script type="text/javascript">
	<?php if(isset($conteudo_extra_texto) and $conteudo_extra_texto){ ?>
	<?php echo html_entity_decode($conteudo_extra_texto, ENT_QUOTES) ?>
	<?php } ?>
</script>
<?php if (\Fuel::$env == \Fuel::PRODUCTION or \Fuel::$env == \Fuel::TEST) { ?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
<?php } ?>
</body>
</html>
