<footer class="footer" id="footer">
   <div class="container">
   <div class="row">
      <div class="col-xs-12 col-md-4">
         <?php echo Asset::img('logobranca.png', array('id' => 'logobranca', 'class' => 'img-responsive')); ?>
         <ul class="lista-footer">
            <li class="li-footer"><a class="ideias-texto" href="#">QUEM SOMOS</a></li>
            <li class="li-footer"><a class="ideias-texto" href="#">COMO TRABALHAMOS</a></li>
            <li class="li-footer"><a class="ideias-texto" href="#">NOSSAS EMPRESAS</a></li>
            <li class=" li-footer"><a class="ideias-texto" href="">EQUIPE</a></li>
            <li class="li-footer"><a class="ideias-texto" href="ideias.php"><strong>IDEIAS EM BUSCA DE DONO </strong></a></li>
         </ul>
      </div>
      <div class="col-xs-12 col-md-4 col-md-offset-4 footer-contato">
         <div class="row">
            <div class="col-xs-12">
               <h2 class="contato-titulo">CONTATO</h2>
			   <br>
               <p>(12) 3456-7890</p>
               <p>ola@iwtc.com</p>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12 icones-redes">
               <a class=" icones-rede icones-redes-footer" href="https://pt-br.facebook.com/"><i class="fab fa fa-facebook-official"></i></a>
               <a class="icones-rede icones-redes-footer" href="https://www.instagram.com/?hl=pt-br"><i class="fab fa fa-instagram"></i></a>
               <a class="icones-rede icones-redes-footer" href="https://www.youtube.com/?gl=BR&amp;hl=pt"><i class="fab fa fa-twitter"></i></a>
               <a class="icones-rede icones-redes-footer" href="https://www.youtube.com/?gl=BR&amp;hl=pt"><i class="fab fa fa-youtube-play"></i></a>
               <a class="icones-rede icones-redes-footer" href="https://www.youtube.com/?gl=BR&amp;hl=pt"><i class="fab fa fa-google-plus-square"></i></a>
            </div>
         </div>
      </div>
   </div>
   <div>
</footer>
<div class="footer-barra">
	<div class="container">
		<p>Copyright 2018 – IWCT Holding. Todos os direitos reservados.</p>
	</div>
</div>
