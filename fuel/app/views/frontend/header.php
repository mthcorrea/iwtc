<nav class="navbar navbar-default navbar-fixed-top nav-custom affix navcustom" >
   <div class="container">
      <div class="row">
         <div class="navbar-header">
            <div class="col-xs-offset-4 col-sm-offset-5 col-md-offset-5 col-lg-offset-0">
               <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarh" aria-controls="navbarh">
               <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>  </button>
               <?php echo Asset::img('logo.png', array('id' => 'logo', 'class' => 'img-responsive logo-header')); ?>
            </div>
         </div>
         <div class="collapse navbar-collapse " id="navbarh">
            <ul class="navigation navbar-center">
               <li><a href="index.php">QUEM SOMOS</a></li>
               <li><a href="index.php">COMO TRABALHAMOS</a></li>
               <li><a href="index.php">NOSSAS EMPRESAS</a></li>
               <li><a href="index.php">EQUIPE</a></li>
               <li class="ideias ideias-destaque"><a class="ideias-texto" href="ideias.php">IDEIAS EM BUSCA DE DONO </a></li>
            </ul>
         </div>
      </div>
   </div>
</nav>
