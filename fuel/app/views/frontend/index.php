<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>IWTC
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="IWTC">
    <?php
if (isset($seo) and is_array($seo)) {
    foreach ($seo as $key => $value) {
        if ($key == 'canonical') {
            echo '<link rel="canonical" href="' . $value . '" />';
        } else {
            $type = (strpos($key, ':') === false) ? 'name' : 'property';
            echo \Html::meta($key, $value, $type);
        }
    }
}
?>

	<script type="text/javascript">
		const BASE_URL = '<?php echo \Uri::create('/'); ?>';
	</script>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo \Uri::create(\Funcoes::auto_version('/assets/css/main.min.css')); ?>">
	<style type="text/css" rel="stylesheet">
		<?php if (isset($conteudo_extra_css) and $conteudo_extra_css) {?>
		<?php echo $conteudo_extra_css ?>
		<?php }?>
	</style>

	<?php echo Asset::js('jquery.min.js'); ?>


</head>
<body>
<?php include 'header.php';?>


<div  id="banner">
<div class="layer1">
<div class="texto-banner">
QUE TAL FAZER SUA EMPRESA DECOLAR?<br>
NÓS SOMOS SEU COMBUSTÍVEL!
</div>
</div>
</div>
		 <?php include 'principal.php';?>
		<?php include 'footer.php';?>

<script src="<?php echo \Uri::create(\Funcoes::auto_version('/assets/js/main.min.js')); ?>"></script>
<script type="text/javascript">
	<?php if (isset($conteudo_extra_texto) and $conteudo_extra_texto) {?>
	<?php echo html_entity_decode($conteudo_extra_texto, ENT_QUOTES) ?>
	<?php }?>
</script>
</body>
</html>
