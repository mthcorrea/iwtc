<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>IWTC
      </title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="IWTC">
      <?php
         if (isset($seo) and is_array($seo)) {
             foreach ($seo as $key => $value) {
                 if ($key == 'canonical') {
                     echo '<link rel="canonical" href="' . $value . '" />';
                 } else {
                     $type = (strpos($key, ':') === false) ? 'name' : 'property';
                     echo \Html::meta($key, $value, $type);
                 }
             }
         }
         ?>
      <script type="text/javascript">
         const BASE_URL = '<?php echo \Uri::create('/'); ?>';
      </script>
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo \Uri::create(\Funcoes::auto_version('/assets/css/main.min.css')); ?>">
      <style type="text/css" rel="stylesheet">
         <?php if (isset($conteudo_extra_css) and $conteudo_extra_css) {?>
         <?php echo $conteudo_extra_css ?>
         <?php }?>
      </style>
      <?php echo Asset::js('jquery.min.js'); ?>
   </head>
   <body>
      <?php include 'header.php';?>
      <div  id="banner">
         <div class="layer2">
            <div class="texto-banner">
               AQUI NA IWTC<br>
               EMPREENDEDORES E INVESTIDORES!
            </div>
         </div>
      </div>
      <div class="container">

               <p class="texto-contato">Gostou de como trabalhamos, tem uma ideia brilhante e não sabe por onde começar?<br> Ou busca uma ideia na qual apostar? Marque uma conversa conosco!</p>
               <p class="texto-contato texto-contato-destaque">Sem compromisso algum, podemos te ouvir e te ajudar a<br> encontrar o <span class="destaque-ideias">piloto ou foguete ideal.</span>
               </h3>
               <div class="col-xs-12 col-md-offset-3 col-md-6">
                  <form class="" method="POST" accept-charset="utf-8">
                     <h4 class="tituloform-contato">Nome</h4>
                     <input class="campo-contato" name="nome" value="" type="text" required="">
                     <h4 class="tituloform-contato">E-mail</h4>
                     <input class="campo-contato" name="email" value="" type="email" required="">
                     <h4 class="tituloform-contato">Telefone</h4>
                     <input class="campo-contato" name="telefone" value="" type="text" required="" maxlength="15">
                     <h4 class="tituloform-contato">Eu sou</h4>
                     <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default">
                           <input type="radio" name="escolha" id="investidor" value="Investidor" autocomplete="off" checked="" required="">
                           <span class="btn-texto">
                              <p>Investidor</p>
                           </span>
                        </label>
                        <label class="btn btn-default active">
                           <input type="radio" name="escolha" id="empreendedor" value="Empreendedor" autocomplete="off" required="">
                           <span class="btn-texto">
                              <p>Empreendedor</p>
                           </span>
                        </label>
                     </div>
                     <h4 class="tituloform-contato">Mensagem</h4>
                     <textarea class="campo-contato text-area" rows="8" name="mensagem" required=""></textarea>
                     <div class="row">
                        <div class="col-md-12">
                           <button type="submit" class="btn-enviar" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Enviar">
                           <strong>Enviar</strong>
                           </button>
                        </div>
                     </div>
                  </form>
               </div>

      </div>
      <?php include 'footer.php';?>
      <script src="<?php echo \Uri::create(\Funcoes::auto_version('/assets/js/main.min.js')); ?>"></script>
      <script type="text/javascript">
         <?php if (isset($conteudo_extra_texto) and $conteudo_extra_texto) {?>
         <?php echo html_entity_decode($conteudo_extra_texto, ENT_QUOTES) ?>
         <?php }?>
      </script>
   </body>
</html>
