<div class="container-fluid sobre-nos">
   <h2 class="titulo-sn">QUEM SOMOS</h2>
   <br>
   <p class="destaque-cinza">Já teve alguma ideia incrível, mas não conseguiu colocá-la no papel? Muito menos tirá-la?<br>
      Ou quer investir e servir de combustível para desenvolver projetos espetaculares?
   </p>
   <br>
   <h3 class="destaque-corpo">
      A IWCT é uma holding que conecta <br><span class="destaque-sn">empreendedores e investidores.</span>
</h3>
   <br>
   <p>Fazemos captação de recursos, vendemos e investimos em ideias estelares,<br>
      em novos negócios e naqueles já monetizados.<br>
      Nossa missão é contribuir com o crescimento das empresas em que investimos,<br>
      formando modelos de negócios viáveis, inovadores e que abrem espaço <br>
      para a monetização e atração de capital.
   </p>
</div>
<div class="como-trabalhamos">
   <div class="container">
      <h2 class="titulo-principal">COMO TRABALHAMOS</h2>
      <div class="column">
         <?php echo Asset::img('sala2.png', array('id' => 'sala-controle', 'class' => 'img-cmt-salacontrole')); ?><br>
         <span class="titulo-cm">SALA DE CONTROLE</span> <br>
         Conversamos para que nos apresente sua ideia.
         <?php echo Asset::img('foguete-trabalhamos.png', array('id' => 'fogue-trabalhamos', 'class' => 'foguete-trabalhamos ')); ?>
      </div>
      <div class="column">
         <figure class="circle circle-primeiro"></figure>
         <span class="titulo-cm">CALIBRAÇÃO</span> <br>
         Encontramos o(s)
         investidor(es) certo(s) para
         ser o seu combustível.
      </div>
      <div class="column" >
         <figure class="circle circle-segundo"></figure>
         <span class="titulo-cm">PREPARAÇÃO</span> <br>
         Juntos, desenvolvemos
         o seu projeto.
      </div>
      <div class="column">
         <div class="decolagem">  <?php echo Asset::img('foguete1.png', array('id' => 'foguete', 'class' => ' img img-foguete')); ?><br>
            <span class="titulo-cm">DECOLAGEM</span> <br>
            Seu foguete está pronto para sair do papel e ir para as alturas!
         </div>
      </div>
   </div>
</div>
<div class="container-fluid section-empresas">
<div class="container-fluid nossas-empresas">
   <h2 class="empresas-titulo">NOSSAS EMPRESAS</h2>
   <p class="empresas-corpo">A IWCT investe e encontra investidores para ideias novas ou já em execução, e também produz<br>
      e vende seus próprios foguetes. Nosso time criativo desenvolve as nossas ideias,<br>
      e estes projetos podem ser seus!<br>
      Estas são todas as aeronaves que ajudamos a lançar ao espaço. Confira:
   </P>
</div>
<div class="container-fluid empresas">
   <div class="carousel" data-flickity='{ "autoPlay": 1500 ,"prevNextButtons": true, "pageDots": false,"watchCSS": true}'>
      <div class="imagem ">
         <?php echo Asset::img('och-investimentos.png', array('id' => 'och', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('hiperion.png', array('id' => 'hiperion', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('logo_valora_imoveis.png', array('id' => 'valora', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('allcw.png', array('id' => 'allco', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('residencial-moscou-logo.png', array('id' => 'campus', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('campus-inc-black.png', array('id' => 'ace', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('ace-startups-logo.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('findme.png', array('id' => 'moscou', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('devdev.png', array('id' => 'campus', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('seniorexp.png', array('id' => 'ace', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('montedog.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('indk.png', array('id' => 'moscou', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('cozy.png', array('id' => 'campus', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('arenamobile.png', array('id' => 'ace', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('gom.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('meugurut.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('teenow.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
      <div class="imagem">
         <?php echo Asset::img('santo-queijo.png', array('id' => 'findme', 'class' => 'img-responsive')); ?>
      </div>
   </div>
</div>
</div>
</div>
<div class="equipe">
   <div class="container">
      <h2 class="equipe-titulo">EQUIPE</h2>

         <div class="col-md-6 col-xs-12">
            <?php echo Asset::img('raphel.png', array('id' => 'raphaeltiseo', 'class' => 'img-responsive')); ?>
         </div>
         <div class="col-md-6 col-xs-12">
            <h2 class="equipe-titulo" >Raphael Tiseo</h2>
            <h3>Investidor e gerente de projetos</h3>
         </div>

   </div>
</div>
<div class="marque-conversa">
     <div class="container-fluid">
      <p class="texto-mc1">Gostou de como trabalhamos, tem uma ideia brilhante e não sabe por onde começar?<br> Ou busca uma ideia na qual apostar?</p>
      <br>
      <p class="texto-mc2"> Marque uma conversa conosco!</p>
      <br>
      <button class="btn btnmarque btnmarque-ideia">TENHO UMA IDEIA
      </button>
      <button class="btn btnmarque btnmarque-investir">QUERO INVESTIR
      </button >
   </div>
</div>
