<?php

class Controller_Frontend extends Controller_Frontend_Template
{
    public function action_index()
    {
        $data = array();

        $this->template->conteudo = \View::forge('frontend/index', $data, false);
    }
    public function action_ideias()
    {
        $data = array();

        $this->template->conteudo = \View::forge('frontend/ideias', $data, false);
    }
}
