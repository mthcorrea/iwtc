<?php
class Breadcrumb_Controle extends \Breadcrumb
{
	private static $_path = 'controle/';

	public static function _init()
	{
		//static::dicionario('painel');
	}

	public static function dicionario($crumb_id, $crumb_link = '')
	{
		\Config::load('breadcrumb', true);
		$crumb_array = \Config::get('breadcrumb.controle.dicionario.' . $crumb_id);

		if (is_array($crumb_array))
		{
			static::add_array_crumb($crumb_array, $crumb_link);
		}
		else
		{
			static::add_crumb($crumb_id, $crumb_link.'/listar');
		}
	}

	public static function add_url_crumb($crumb_url)
	{
		$crumbs = explode('/', substr($crumb_url, strlen(Uri::base() . static::$_path)));

		$crumb_link = trim(static::$_path, '/');

		foreach ($crumbs as $crumb) {
			$crumb_link .= '/'.$crumb;

			if (is_numeric($crumb))
			{
				continue;
			}

			static::dicionario($crumb, $crumb_link);
		}
	}

	public static function add_array_crumb($crumb_array, $crumb_link = '')
	{
		$crumb_label = $crumb_array['label'];

		if (isset($crumb_array['icon']))
			$crumb_label = html_tag('span', array('class' => $crumb_array['icon']), '') . " " . $crumb_label;

		if (isset($crumb_array['link']))
			$crumb_link = static::$_path . $crumb_array['link'];

		static::add_crumb($crumb_label, $crumb_link);
	}
}
