<?php
class Observer_Controle extends \Orm\Observer
{

    public function before_delete($obj)
    {
    	/* deleta todos os arquivos (img) upload */
    	$caminho_upload = Config::get('upload.path');

    	foreach ($obj as $propriedade => $valor)
    	{
    		if ( preg_match('/^img[0-9]?/', $propriedade) or preg_match('/^logo[0-9]?/', $propriedade) )
    		{
    			$arquivos = File::read_dir($caminho_upload, 0, array(
				    '!^\.',
				    "^$valor\$" => 'file',
				));

    			foreach ($arquivos as $pasta => $arquivo)
    			{

    				$caminho_completo = null;

                    if (is_numeric($pasta))
    				{
    					$caminho_completo = $caminho_upload.$arquivo;
    				}
    				else
    				{
    					if (count($arquivo) == 1)
    					{
    						$caminho_completo = $caminho_upload.$pasta.$arquivo[0];
    					}
    				}

                    if ( ! is_null($caminho_completo) and file_exists($caminho_completo))
                    {
                        \File::delete($caminho_completo);
                    }
    			}
    		}
            elseif (preg_match('/^arquivo[0-9]?/', $propriedade))
            {
                $caminho_completo = $caminho_upload.$valor;
                if ( ! empty($valor) and file_exists($caminho_completo))
                {
                    \File::delete($caminho_completo);
                }
            }
    	}

    }
}