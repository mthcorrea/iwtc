<?php
class Auth_Login_Ormauth extends \Auth\Auth_Login_Ormauth
{

	public function validate_user($username_or_email = '', $password = '')
	{
		// get the user identification and password
		$username_or_email = trim($username_or_email) ?: trim(\Input::post(\Config::get('ormauth.username_post_key', 'username')));
		$password = trim($password) ?: trim(\Input::post(\Config::get('ormauth.password_post_key', 'password')));

		// and make sure we have both
		if (empty($username_or_email) or empty($password))
		{
			return false;
		}

		// hash the password
		$password = $this->hash_password($password);

		// and do a lookup of this user
		$user = \Model\Auth_User::query()
			->select(\Config::get('ormauth.table_columns', array()))
			->related('metadata')
			->where_open()
				->where('username', '=', $username_or_email)
				->or_where('email', '=', $username_or_email)
			->where_close()
			->where('password', '=', $password)
			->where('status', 1)
			->get_one();

		// return the user object, or false if not found
		return $user ?: false;
	}

	/**
	 * Update a user's properties
	 * Note: Username cannot be updated, to update password the old password must be passed as old_password
	 *
	 * @param   Array  properties to be updated including profile fields
	 * @param   string
	 * @return  bool
	 */
	public function update_user($values, $username = null)
	{
		// if no username is given, fetch the current user's namd
		$username = $username ?: $this->user->username;

		// get the current user record
		$current_values = \Model\Auth_User::query()
			->select(\Config::get('ormauth.table_columns', array()))
			->where('username', '=', $username)
			->get_one();

		// and bail out if it doesn't exist
		if (empty($current_values))
		{
			throw new \SimpleUserUpdateException('Username not found', 4);
		}

		// validate the values passed and assume the update array
		$update = array();
		if (array_key_exists('username', $values))
		{
			throw new \SimpleUserUpdateException('Username cannot be changed.', 5);
		}
		if (array_key_exists('password', $values))
		{
			if (empty($values['old_password'])
				or $current_values->password != $this->hash_password(trim($values['old_password'])))
			{
				throw new \SimpleUserWrongPassword('Old password is invalid');
			}

			$password = trim(strval($values['password']));
			if ($password === '')
			{
				throw new \SimpleUserUpdateException('Password can\'t be empty.', 6);
			}
			$update['password'] = $this->hash_password($password);
			unset($values['password']);
		}
		if (array_key_exists('old_password', $values))
		{
			unset($values['old_password']);
		}
		if (array_key_exists('email', $values))
		{
			$email = filter_var(trim($values['email']), FILTER_VALIDATE_EMAIL);
			if ( ! $email)
			{
				throw new \SimpleUserUpdateException('Email address is not valid', 7);
			}

			$matches = \Model\Auth_User::query()
				->select(\Config::get('ormauth.table_columns', array()))
				->where('email', '=', $email)
				->where('id', '!=', $current_values->id)
				->get_one();

			if ($matches)
			{
				throw new \SimpleUserUpdateException('Email address is already in use', 11);
			}

			$update['email'] = $email;
			unset($values['email']);
		}
		// deal with some simpleauth compatibility
		if (array_key_exists('group', $values))
		{
			array_key_exists('group_id', $values) or $values['group_id'] = $values['group'];
			unset($values['group']);
		}
		if (array_key_exists('group_id', $values))
		{
			if (is_numeric($values['group_id']))
			{
				$update['group_id'] = (int) $values['group_id'];
			}
			unset($values['group_id']);
		}

		// load the updated values into the object
		$current_values->from_array($update);

		$updated = false;

		// any values remaining?
		if ( ! empty($values))
		{
			// set them as EAV values
			foreach ($values as $key => $value)
			{
				if ( ! isset($current_values->{$key}) or $current_values->{$key} != $value)
				{
					if ($value === null)
					{
						unset($current_values->{$key});
					}
					else
					{
						$current_values->{$key} = $value;
					}

					// mark we've updated something
					$updated = true;
				}
			}
		}

		// check if this has changed anything
		if ($updated or $updated = $current_values->is_changed())
		{
			// and only save if it did
			$current_values->save();
		}

		// return the updated status
		return $updated;
	}

	/**
	 * Generates new random password, sets it for the given username and returns the new password.
	 * To be used for resetting a user's forgotten password, should be emailed afterwards.
	 *
	 * @param   string  $username
	 * @return  string
	 */
	public function reset_password($username)
	{
		// get the user object
		$user = \Model\Auth_User::query()
			->select(\Config::get('ormauth.table_columns', array()))
			->where('username', '=', $username)
			->get_one();

		// and bail out if not found
		if ( ! $user)
		{
			throw new \SimpleUserUpdateException('Failed to reset password, user was invalid.', 8);
		}

		// generate a new random password
		$new_password = \Str::random('alnum', 8);
		$user->password = $this->hash_password($new_password);

		// store the updated password hash
		$user->save();

		// and return the new password
		return $new_password;
	}

	/**
	 * Deletes a given user
	 *
	 * @param   string
	 * @return  bool
	 */
	public function delete_user($username)
	{
		// make sure we have a user to delete
		if (empty($username))
		{
			throw new \SimpleUserUpdateException('Cannot delete user with empty username', 9);
		}

		// get the user object
		$user = \Model\Auth_User::query()
			->related('metadata')
			->select(\Config::get('ormauth.table_columns', array()))
			->where('username', '=', $username)
			->get_one();

		// if it was found, delete it
		if ($user)
		{
			return $user->delete();
		}
		else
		{
			return false;
		}
	}

	/**
	 * Check for login
	 *
	 * @return  bool
	 */
	protected function perform_check()
	{
		// get the username and login hash from the session
		$username    = \Session::get('username');
		$login_hash  = \Session::get('login_hash');

		// only worth checking if there's both a username and login-hash
		if ( ! empty($username) and ! empty($login_hash))
		{
			// if we don't have a user, or we're logging in from guest mode
			if (is_null($this->user) or ($this->user->username != $username and $this->user->id == 0))
			{
				// find the user
				$this->user = \Model\Auth_User::query()
					->select(\Config::get('ormauth.table_columns', array()))
					->related('metadata')
					->where('username', '=', $username)
					->get_one();
			}

			// return true when login was verified, and either the hash matches or multiple logins are allowed
			if ($this->user and (\Config::get('ormauth.multiple_logins', false) or $this->user['login_hash'] === $login_hash))
			{
				return true;
			}
		}

		// not logged in, do we have remember-me active and a stored user_id?
		elseif (static::$remember_me and $user_id = static::$remember_me->get('user_id', null))
		{
			return $this->force_login($user_id);
		}

		// force a logout
		$this->logout();

		return false;
	}
}