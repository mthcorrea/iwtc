<?php
// Ugly temporary windows fix because windows doesn't support strptime()
// It attempts conversion between glibc style formats and PHP's internal style format (no 100% match!)
if (!function_exists('strptime') && !function_exists('Fuel\Core\strptime')) {
    function strptime($input, $format)
    {
        // convert the format string from glibc to date format (where possible)
        $new_format = str_replace(
            array('%a', '%A', '%d', '%e', '%j', '%u', '%w', '%U', '%V', '%W', '%b', '%B', '%h', '%m', '%C', '%g', '%G', '%y', '%Y', '%H', '%k', '%I', '%l', '%M', '%p', '%P', '%r', '%R', '%S', '%T', '%X', '%z', '%Z', '%c', '%D', '%F', '%s', '%x', '%n', '%t', '%%'),
            array('D', 'l', 'd', 'j', 'N', 'z', 'w', '[^^]', 'W', '[^^]', 'M', 'F', 'M', 'm', '[^^]', 'Y', 'o', 'y', 'Y', 'H', 'G', 'h', 'g', 'i', 'A', 'a', 'H:i:s A', 'H:i', 's', 'H:i:s', '[^^]', 'O', 'T ', '[^^]', 'm/d/Y', 'Y-m-d', 'U', '[^^]', "\n", "\t", '%'),
            $format
        );

        // parse the input
        $parsed = date_parse_from_format($new_format, $input);

        // parse succesful?
        if (is_array($parsed) and empty($parsed['errors'])) {
            return array(
                'tm_year' => $parsed['year'] - 1900,
                'tm_mon' => $parsed['month'] - 1,
                'tm_mday' => $parsed['day'],
                'tm_hour' => $parsed['hour'] ?: 0,
                'tm_min' => $parsed['minute'] ?: 0,
                'tm_sec' => $parsed['second'] ?: 0,
            );
        } else {
            $masks = array(
                '%d' => '(?P<d>[0-9]{2})',
                '%m' => '(?P<m>[0-9]{2})',
                '%Y' => '(?P<Y>[0-9]{4})',
                '%H' => '(?P<H>[0-9]{2})',
                '%M' => '(?P<M>[0-9]{2})',
                '%S' => '(?P<S>[0-9]{2})',
            );

            $rexep = "#" . strtr(preg_quote($format), $masks) . "#";

            if (!preg_match($rexep, $input, $result)) {
                return false;
            }

            return array(
                "tm_sec" => isset($result['S']) ? (int)$result['S'] : 0,
                "tm_min" => isset($result['M']) ? (int)$result['M'] : 0,
                "tm_hour" => isset($result['H']) ? (int)$result['H'] : 0,
                "tm_mday" => isset($result['d']) ? (int)$result['d'] : 0,
                "tm_mon" => isset($result['m']) ? ($result['m'] ? $result['m'] - 1 : 0) : 0,
                "tm_year" => isset($result['Y']) ? ($result['Y'] > 1900 ? $result['Y'] - 1900 : 0) : 0,
            );
        }
    }
}

/**
 * Date Class
 *
 * DateTime replacement that supports internationalization and does correction to GMT
 * when your webserver isn't configured correctly.
 *
 * @package     Fuel
 * @subpackage  Core
 * @category    Core
 * @link        http://docs.fuelphp.com/classes/date.html
 *
 * Notes:
 * - Always returns Date objects, will accept both Date objects and UNIX timestamps
 * - create_time() uses strptime and has currently a very bad hack to use strtotime for windows servers
 * - Uses strftime formatting for dates www.php.net/manual/en/function.strftime.php
 */
class Date extends \Fuel\Core\Date
{
    /**
     * Uses the date config file to translate string input to timestamp
     *
     * @param   string  date/time input
     * @param   string  key name of pattern in config file
     * @return  Date
     */
    public static function create_from_string($input, $pattern_key = 'local')
    {
        $timestamp = null;
        if ($input) {
            \Config::load('date', 'date');

            $pattern = \Config::get('date.patterns.' . $pattern_key, null);
            empty($pattern) and $pattern = $pattern_key;

            $time = strptime($input, $pattern);
            if ($time === false) {
                throw new \UnexpectedValueException('Input was not recognized by pattern.');
            }

            $timestamp = mktime($time['tm_hour'], $time['tm_min'], $time['tm_sec'],
                $time['tm_mon'] + 1, $time['tm_mday'], $time['tm_year'] + 1900);
            if ($timestamp === false) {
                throw new \OutOfBoundsException('Input was invalid.' . (PHP_INT_SIZE == 4 ? ' A 32-bit system only supports dates between 1901 and 2038.' : ''));
            }
        }
        return static::forge($timestamp);
    }
}
