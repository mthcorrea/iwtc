<?php
class Request extends \Fuel\Core\Request
{
	/*
	* Verifica se é o action ativo.
	*/
	public static function is_this_action($action)
	{
		return (static::active()->action == $action);
	}

	/**
	 * Exemplo: action_exists('adicionar') - Ele pega o controller atual
	 * Exemplo: action_exists('controller','adicionar') - Ele verifica no 'controller'
	 */

	public static function action_exists($controller = null, $action = null)
	{
		if ($controller)
		{
			if ($action == null)
			{
				$action = $controller;
				$controller = static::active()->controller;
			}
			return method_exists($controller, 'action_'.$action);
		}
		return false;
	}

}