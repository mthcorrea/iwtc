<?php

class Casset extends \Casset\Casset
{
	/**
	 * Figures out where a file should be, based on its namespace and type.
	 *
	 * @param string $file The name of the asset to search for
	 * @param string $asset_type 'css', 'js' or 'img'
	 * @return string The path to the asset, relative to $asset_url
	 */
	protected static function find_files($file, $asset_type)
	{
		$parts = explode('::', $file, 2);
		if (!array_key_exists($parts[0], static::$asset_paths))
			throw new Casset_Exception("Could not find namespace {$parts[0]}");

		$path = static::$asset_paths[$parts[0]]['path'];
		$file = $parts[1];

		if ($file == '')
		{
			return array();
		}

		$folder = $file[0] == '/' ? '' : static::$asset_paths[$parts[0]]['dirs'][$asset_type];
		$file = ltrim($file, '/');

		$remote = (strpos($path, '//') !== false);

		if ($remote)
		{
			// Glob doesn't work on remote locations, so just assume they
			// specified a file, not a glob pattern.
			// Don't look for the file now either. That'll be done by
			// file_get_contents later on, if need be.
			return array($path.$folder.$file);
		}
		else
		{
			$default_path = str_replace(\Config::get('site.dominio'), 'default', $path);
			if (in_array($asset_type, array('js', 'css', 'img')) and ! file_exists($path.$folder.$file))
			{
				$path = $default_path;
			}
			$glob_files = array_filter(glob($path.$folder.$file), 'is_file');
			return $glob_files;
		}
	}

	public static function img_exists($image)
	{
		if (strpos($image, '::') === false)
			$image = static::$default_path_key.'::'.$image;

		$image_paths = static::find_files($image, 'img');

		return count($image_paths) > 0;
	}

	/**
	 * Locates the given image(s), and returns the resulting <img> tag.
	 *
	 * @param mixed $images Image(s) to print. Can be string or array of strings
	 * @param string $alt The alternate text
	 * @param array $attr Attributes to apply to each image (eg width)
	 * @return string The resulting <img> tag(s)
	 */
	public static function img($images, $alt = '', $attr = array())
	{
		if (!is_array($images))
			$images = array($images);
		$attr['alt'] = $alt;
		$ret = '';
		foreach ($images as $image)
		{
			if (strpos($image, '::') === false)
				$image = static::$default_path_key.'::'.$image;
			$image_paths = static::find_files($image, 'img');
			foreach ($image_paths as $image_path)
			{
				$remote = (strpos($image_path, '//') !== false);
				$abs_image_path = $image_path;
				$image_path = static::process_filepath($image_path, 'img', $remote);
				$base = ($remote) ? '' : static::$asset_url;

				// Image size
				try {
					list($width, $height) = getimagesize(($remote) ? $base.$image_path : DOCROOT.$abs_image_path);
					$attr['width']  = (isset($attr['width']) ? $attr['width'] : $width);
					$attr['height'] = (isset($attr['height']) ? $attr['height'] : $height);
				} catch (\Exception $e) {
					// echo $e->getMessage();
				}
				// End Image size

				$attr['src']    = $base.$image_path;
				$ret .= html_tag('img', $attr);
			}
		}
		return $ret;
	}
}

/* End of file casset.php */
