<?php
class Video
{
    public static function decode_links($video_string)
    {
        $videos = explode(';', $video_string);
        $videos = array_map('trim', $videos);
        return $videos;
    }

    public static function get_video_type($video_url)
    {
    	if (strpos($video_url, 'youtu') !== false) {
    		return 'yt';
    	}
    	if (strpos($video_url, 'vimeo') !== false) {
    		return 'vm';
    	}
    	return false;
    }

    /* Vimeo */

    /* YouTube */
    public static function get_yt_video_id($video_url)
    {
        $parse_url = parse_url($video_url);
        if (isset($parse_url['query']))
        {
            parse_str($parse_url['query'], $args);
            foreach ($args as $key => $arg) {
                unset($args[$key]);
                $key = str_replace(array('&', '&amp;', 'amp;'), '', $key);
                $args[$key] = $arg;
            }
            return isset($args['v']) ? $args['v'] : false;
        }
        else
        {
            return trim($parse_url['path'], '/');
        }
    }
    public static function get_yt_video_info($video_url)
    {
        $video_id = static::get_yt_video_id($video_url);
        if ($video_id)
        {
            $video_info = array();
            $video_info['link'] = 'https://www.youtube.com/embed/'.$video_id;
            $video_info['imagem'] = 'http://i.ytimg.com/vi/'.$video_id.'/hqdefault.jpg';
            return $video_info;
            /*
            $json = file_get_contents('http://gdata.youtube.com/feeds/api/videos/'.$video_id.'?v=2&alt=jsonc');
            $info = json_decode($json);
            if ( ! isset($info->error) and isset($info->data))
            {
                $video_info = array();
                $video_info['titulo'] = $info->data->title;
                $video_info['link'] = 'https://www.youtube.com/embed/'.$info->data->id;
                $video_info['duracao'] = floor($info->data->duration/60).':'.($info->data->duration % 60);
                $video_info['descricao'] = $info->data->description;
                $video_info['imagem'] = $info->data->thumbnail->hqDefault;
                return $video_info;
            }
            */
        }
        return false;
    }
    public static function link_embed_yt($video_url)
    {
    	$video_info = static::get_yt_video_info($video_url);
    	if ($video_info)
    	{
    		return $video_info['link'];
    	}
    	return false;
    }
    public static function embed_yt($video_url, $width, $height)
    {
    	$video_info = static::get_yt_video_info($video_url);
    	if ($video_info)
    	{
    		return '<iframe width="'.$width.'" height="'.$height.'" src="'.$video_info['link'].'" frameborder="0" allowfullscreen></iframe>';
    	}
    	return false;
    }
    public static function thumb_yt($video_url)
    {
    	$video_info = static::get_yt_video_info($video_url);
    	if ($video_info)
    	{
    		return $video_info['imagem'];
    	}
    	return false;
    }

    public static function link_embed($video_url)
    {
    	$video_type = static::get_video_type($video_url);
    	if ($video_type) {
    		$function = "link_embed_$video_type";
    		return static::$function($video_url);
    	}
    	return false;
    }

    public static function embed($video_url, $width, $height)
    {
    	$video_type = static::get_video_type($video_url);
    	if ($video_type) {
    		$function = "embed_$video_type";
    		return static::$function($video_url, $width, $height);
    	}
    	return false;
    }

    public static function thumb($video_url)
    {
    	$video_type = static::get_video_type($video_url);
    	if ($video_type) {
    		$function = "thumb_$video_type";
    		return static::$function($video_url);
    	}
    	return false;
    }
}