<?php
/**
 * Menu Class for FuelPHP, with permissions
 *
 * @version    1
 * @author     Heitor Figueiredo - @heitorfig
 * @link
 */

/**
 * Menu Class
 *
 * Lightweight class to create menu based on array
 *
 */
class Menu
{
	/**
	 * @var  Menu
	 */
	protected static $_instance;

	/**
	 * @var  Uri Path
	 */
	protected static $_path_uri;

	/**
	 * @var  array  contains references to all instantiations of Menu
	 */
	protected static $_instances = array();

	/**
	 * Create Fieldset object
	 *
	 * @param   string    Identifier for this fieldset
	 * @param   array     Configuration array
	 * @return  Fieldset
	 */
	public static function forge($name = 'default', $items = array(), $path_uri = null)
	{
		if ($exists = static::instance($name)) {
			\Error::notice('Este menu já existe, não pode ser sobrescrito.');
			return $exists;
		}

		static::$_instances[$name] = new static($items);
		static::$_path_uri = $path_uri;

		if ($name == 'default') {
			static::$_instance = static::$_instances[$name];
		}

		return static::$_instances[$name];
	}

	/**
	 * Return a specific instance, or the default instance (is created if necessary)
	 *
	 * @param   string  driver id
	 * @return  Fieldset
	 */
	public static function instance($instance = null)
	{
		if ($instance !== null) {
			if ( ! array_key_exists($instance, static::$_instances)) {
				return false;
			}
			return static::$_instances[$instance];
		}

		if (static::$_instance === null) {
			static::$_instance = static::forge();
		}

		return static::$_instance;
	}

	/**
	 * @var  array  Menu items
	 */
	protected $menu = array();

	public function __construct($items = array())
	{
		$this->menu = $items;
	}

	/**
	 * Return the html of the menu
	 *
	 * @return  HTML code
	 */
	public function render()
	{
		if ( empty($this->menu)) {
			return '';
		}

		$output = $this->menu($this->menu, true);

		return $output;
	}

	/**
	 * Return the html code of the menu.
	 * Heads Up: This function is recursive.
	 *
	 * @param   array  Menu array
	 * @param   bool  Is it the main level?
	 * @return  HTML code
	 */
	protected function menu($menu, $main = false)
	{
		$ret = null;
		/*if (is_array($menu)) {
			$menu_attr = null;
			isset($menu['attr']) and $menu_attr = $menu['attr'];
			if (isset($menu['navs']) and $menu['navs'] and is_array($menu['navs'])) {
				$uls = null;
				foreach ($menu['navs'] as $ul) {
					$ul_attr = null;
					isset($ul['attr']) and $ul_attr = $ul['attr'];
					$lis = null;
					if (isset($ul['attr']) and $ul['itens'] and is_array($ul['itens']))
					{
						foreach ($ul['itens'] as $li) {
							if (isset($li['permission']) and isset($menu['permission'])) {
								if ( ! Auth::has_access($li['permission']))
									continue;
							}

							$li_attr = null;
							isset($li['attr']) and $li_attr = $li['attr'];

							$link_attr = array();

							if ( ! isset($li['link']) or $li['link'] == '')
							{
								$a = $li['label'];
							}
							else
							{
								$link = $li['link'];
								if (is_array($li['link'])) {
									$link = $li['link']['uri'];
									$link_attr = $li['link']['attr'];
								}
								if (strpos('://', $link) < 0)
									$link = static::$_path_uri . $link;

								if (Uri::create($link) == Uri::create()) {
									$li_attr['class'] or $li_attr['class'] = null;
									$li_attr['class'] .= ' active';
								}

								$a = Html::anchor($link, $li['label'], $link_attr);
							}

							$lis .= html_tag('li', $li_attr, $a);
						}
					}
					$uls .= html_tag('ul', $ul_attr, $lis);
				}
			}
			$ret = html_tag('nav', $menu_attr, $uls);
		}*/
		if ($menu and is_array($menu)) {
			isset($menu['attr']) or $menu['attr'] = array();
			isset($menu['navs']) or $menu['navs'] = null;

			if (is_array($menu['navs'])) {
				$navs = null;

				foreach ($menu['navs'] as $nav) {
					$navs .= $this->menu_items($nav);
				}

				$ret = html_tag('nav', $menu['attr'], $navs);
			}
		}
		return $ret;
	}

	protected function menu_items($ul)
	{
		isset($ul['attr']) or $ul['attr'] = array();
		isset($ul['itens']) or $ul['itens'] = null;

		if (is_array($ul['itens']))
		{
			$items = null;

			foreach ($ul['itens'] as $li)
			{

				if (isset($li['permission']) and ! Auth::has_access($li['permission']))
					continue;

				isset($li['label']) or $li['label'] = null;
				isset($li['link']) or $li['link'] = null;
				isset($li['attr']) or $li['attr'] = array();
				isset($li['attr']['class']) or $li['attr']['class'] = null;

				$item = $li['label'];

				if ($li['link']) {
					$link = $li['link'];
					$li['link'] = array();
					isset($li['link']['uri']) or $li['link']['uri'] = $link;
					isset($li['link']['attr']) or $li['link']['attr'] = array();
					isset($li['link']['attr']['class']) or $li['link']['attr']['class'] = null;

					if (strpos('://', $li['link']['uri']) === false and $li['link']['uri'] != '#')
						$li['link']['uri'] = static::$_path_uri . $link;

					if (Uri::create($li['link']['uri']) == Uri::create())
						$li['attr']['class'] .= ' active';

					if (isset($li['dropdown'])) {
						$li['link']['attr']['class'] .= ' dropdown-toggle';
						$li['link']['attr']['data-toggle'] = 'dropdown';
						$li['label'] .= ' <span class="caret"></span>';
					}

					$item = Html::anchor($li['link']['uri'], $li['label'], $li['link']['attr']);
				}

				if (isset($li['dropdown']) and $li['dropdown'] and ! is_null($li['dropdown']))
				{
					$li['attr']['class'] .= ' dropdown';
					$item .= $this->menu_items($li['dropdown']);
				}

				if (isset($li['dropdown_submenu']) and $li['dropdown_submenu'] and ! is_null($li['dropdown_submenu']))
				{
					$li['attr']['class'] .= ' dropdown-submenu';
					$item .= $this->menu_items($li['dropdown_submenu']);
				}

				$items .= html_tag('li', $li['attr'], $item);
			}

			return html_tag('ul', $ul['attr'], $items);
		}
	}
}