<?php

/**
 * TableSort class which creates <head> section with all the columns and sorting options html for the <table> tag
 */
class TableSort {

	/**
	 * @var string Default/current sort field
	 */
	public static $sort_by 			= '';

	/**
	 * @var string Default/current sort direction
	 */
	public static $direction 		= '';

	/**
	 * @var array or string Array of columns - attributes: (sort_key, [column_name], [array attributes])
	 * Use string for column with no sorting
	 */
	public static $columns = array(
		array('id', 'id', array())
	);

	/**
	 * @var array The HTML for the display
	 */
	public static $template = array(
		'wrapper_start'		=> '<thead>',
		'wrapper_end'		=> '</thead>',
		'col_tag'			=> 'th',
		'col_class_active'  => 'sorting',
		'link_start'  		=> '<a>',
		'link_end'    		=> '</a>',
		'nolink_start'		=> '<span>',
		'nolink_end'		=> '</span>',
	);

	/**
	 * @var string Separator for concatenating sorty_by and direction in URL
	 */
	protected static $uri_delimiter	= '-';

	/**
	 * @var	integer	The URI segment containg sorty_by and direction keys
	 */
	protected static $uri_segment = 'sort';

	/**
	 * @var	string	Base url for sorter links
	 */
	protected static $pagination_url = '';

	/**
	 * @var	boolean	Use cookies to remember current sort_by and direction
	 */
	protected static $use_cookies = false;

	/**
	 * @var	integer	Default sort by order, stored for the reset() method
	 */
	protected static $default_sort_by = 'id';

	/**
	 * @var	integer	Default sort direction, stored for the reset() method
	 */
	protected static $default_direction = 'desc';

	/**
	 * Init
	 *
	 * Loads in the config and sets the variables
	 *
	 * @access	public
	 * @return	void
	 */
	public static function _init()
	{
		$config = \Config::get('tablesort', array());

		static::set_config($config);
	}

	/**
	 * configuration value getter
	 */
	public static function get($name)
	{
		if (isset(static::${$name}))
		{
			return static::${$name};
		}
		elseif (isset(static::$template[$name]))
		{
			return static::$template[$name];
		}
		else
		{
			return null;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Set Config
	 *
	 * Sets the configuration for tablesort
	 *
	 * @access public
	 * @param array   $config The configuration array
	 * @return void
	 */
	public static function set_config(array $config)
	{
		foreach ($config as $key => $value)
		{
			if ($key == 'template')
			{
				static::$template = array_merge(static::$template, $config['template']);
				continue;
			}

			if (isset(static::${$key}))
			{
			    static::${$key} = $value;
			}

		}

		static::initialize();
	}

	// --------------------------------------------------------------------

	/**
	 * Reset variables
	 *
	 * Reset variables to default and delete cookies
	 *
	 * @access public
	 * @param array   $config The configuration array
	 * @return void
	 */
	public static function reset()
	{
		if(static::$use_cookies === true)
		{
			\Cookie::delete('fuel_ts_sort_by');
			\Cookie::delete('fuel_ts_direction');
		}

		static::$sort_by 	= static::$default_sort_by;
		static::$direction 	= static::$default_direction;
	}

	// --------------------------------------------------------------------

	/**
	 * Prepares vars for creating table head elements
	 *
	 * @access protected
	 * @return void
	 */
	protected static function initialize()
	{

		// Set default sort order & so we can use it for reset() function
		static::$default_sort_by 	= static::$sort_by;
		static::$default_direction 	= static::$direction;

		if (is_string(static::$uri_segment))
		{
				$sort_uri = \Input::get(static::$uri_segment);
		}
		else
		{
				$sort_uri = \URI::segment(static::$uri_segment);
		}

		if($sort_uri && strpos($sort_uri, static::$uri_delimiter) !== false)
		{
			$sort_uri = explode(static::$uri_delimiter, $sort_uri);
			static::$sort_by 	= static::_exists_sort_by($sort_uri[0]);
			static::$direction 	= static::_exists_direction($sort_uri[1]);

			if(static::$use_cookies === true)
			{
				$cookie_url = static::$pagination_url;
				\Cookie::set('fuel_ts_sort_by', static::$sort_by, null, $cookie_url);
				\Cookie::set('fuel_ts_direction', static::$direction, null, $cookie_url);
			}
		}
		elseif (static::$use_cookies === true)
		{
			static::$sort_by 	= \Cookie::get('fuel_ts_sort_by', static::$sort_by);
			static::$direction 	= \Cookie::get('fuel_ts_direction', static::$direction);
		}

	}

	// --------------------------------------------------------------------

	/**
	 * Creates table sort head elements
	 *
	 * @access public
	 * @return mixed Table sort html elements
	 */
	public static function create_table_head()
	{

		if (!static::$columns)
		{
			return static::$template['wrapper_start'].'<tr><th>Sem configuração de colunas</th></tr>'.static::$template['wrapper_end'];
		}

		$table_head  = static::$template['wrapper_start'];
		$table_head .= '<tr>';

		foreach(static::$columns as $column)
		{
			$sort_key 	= (is_array($column) ? isset($column[1]) ? $column[1] : \Str::lower($column[0]) : $column);
			$col_attr	= (is_array($column) && isset($column[2]) ? $column[2] : array());

			$new_direction = static::$direction;

			if(static::$sort_by == $sort_key)
			{
				$active_class_name = static::$template['col_class_active'].' '.static::$template['col_class_active'].'-'.$new_direction;
				if(isset($col_attr['class']))
				{
					$col_attr['class'] .= ' '.$active_class_name;
				}
				else
				{
					$col_attr['class'] = $active_class_name;
				}

				$new_direction = (static::$direction == 'asc' ? 'desc' : 'asc');

			}

			if(is_array($column) && (!isset($column[1]) || isset($column[1]) && $column[1] !== false)){

				if ($sort_key == '')
				{
					$cell_content 	= $column[0];
				}
				else
				{
					$cell_content 	= rtrim(static::$template['link_start'], '> ').' href="'.static::_make_link($sort_key,$new_direction).'">';
					$cell_content 	.= $column[0];
					$cell_content 	.= static::$template['link_end'];
				}
			}else{
				if(is_array($column))
				{
					$column = $column[0];
				}
				$cell_content = static::$template['nolink_start'].$column.static::$template['nolink_end'];
			}

			$table_head .= html_tag(static::$template['col_tag'], $col_attr, $cell_content);

		}

		$table_head .= '</tr>';
		$table_head .= static::$template['wrapper_end'];

		return $table_head;
	}

	protected static function _exists_direction($direction)
	{

		if (in_array($direction, array('asc', 'desc')))
		{
			return $direction;
		}

		return static::$direction;
	}

	protected static function _exists_sort_by($sort_by)
	{
		foreach (static::$columns as $column)
		{
			if ($column[1] == $sort_by)
			{
				return $sort_by;
			}
		}

		return static::$sort_by;
	}

	protected static function _make_link($sort_key,$direction)
	{

		// construct a pagination url if we don't have one
		if (is_null(static::$pagination_url))
		{
			// start with the main uri
			static::$pagination_url = \Uri::main();
			\Input::get() and static::$pagination_url .= '?'.http_build_query(\Input::get());
		}


		// break the url in bits so we can insert it
		$url = parse_url(static::$pagination_url);

		// parse the query string
		if (isset($url['query']))
		{
			parse_str($url['query'], $url['query']);
		}
		else
		{
			$url['query'] = array();
		}

		// do we have a segment offset due to the base_url containing segments?
		$seg_offset = parse_url(rtrim(\Uri::base(), '/'));
		$seg_offset = empty($seg_offset['path']) ? 0 : count(explode('/', trim($seg_offset['path'], '/')));

		// is the page number a URI segment?
		if (is_numeric(static::$uri_segment))
		{
			// get the URL segments
			$segs = isset($url['path']) ? explode('/', trim($url['path'], '/')) : array();

			// do we have enough segments to insert? we can't fill in any blanks...
			if (count($segs) < static::$uri_segment - 1)
			{
				throw new \RuntimeException("Not enough segments in the URI");
			}

			// replace the selected segment with the page placeholder
			$segs[static::$uri_segment - 1 + $seg_offset] = '{tablesort}';
			$url['path'] = '/'.implode('/', $segs);
		}
		else
		{
			// add our placeholder
			$url['query'][static::$uri_segment] = '{tablesort}';
		}

		// re-assemble the url
		$query = empty($url['query']) ? '' : '?'.preg_replace('/%7Btablesort%7D/', '{tablesort}', http_build_query($url['query']));
		unset($url['query']);
		empty($url['scheme']) or $url['scheme'] .= '://';
		empty($url['port']) or $url['host'] .= ':';
		static::$pagination_url = implode($url).$query;


		// return the page link
		return str_replace('{tablesort}', $sort_key.static::$uri_delimiter.$direction, static::$pagination_url);
	}

}


