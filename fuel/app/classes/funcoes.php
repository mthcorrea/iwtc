<?php

/* Se não existe no PHP, cria a função */
if (!function_exists('mb_unserialize')) {
    function mb_unserialize($string)
    {
        $string2 = preg_replace_callback(
            '!s:(\d+):"(.*?)";!s',
            function ($m) {
                $len = strlen($m[2]);
                $result = "s:$len:\"{$m[2]}\";";
                return $result;

            },
            $string);
        return unserialize($string2);
    }
}

/* Se não existe no PHP, cria a função */
if (!function_exists('mb_str_word_count')) {
    function mb_str_word_count($string, $format = 0, $charlist = '[]')
    {
        $string = trim($string);
        if (empty($string))
            $words = array();
        else
            $words = preg_split('~[^\p{L}\p{N}\']+~u', $string);
        switch ($format) {
            case 0:
                return count($words);
                break;
            case 1:
            case 2:
                return $words;
                break;
            default:
                return $words;
                break;
        }
    }
}

class Funcoes
{
    /**
     * FAz o upload de imagens e redimensiona de acordo com a variável $tamanhos
     * @param  array $campos listagem dos nomes de campos
     * @param  ponteiro $registro
     * @param  object $menu
     * @param  multiarray $tamanhos [description]
     * @return none
     */
    public static function upload_varios_tamanhos($campos, &$registro, $tamanhos = array())
    {
        if (is_null($tamanhos)) {
            die('informe os tamanhos');
        }

        if (is_array(\Input::file()) and count(\Input::file()) > 0) {
            \Upload::process();

            if (\Upload::is_valid()) {
                $files = \Upload::get_files();
                foreach ($files as $key_file => $file) {
                    if (in_array($file['field'], $campos)) {
                        /* Upload */
                        \Upload::save($key_file);
                        $file = \Upload::get_files($key_file);


                        foreach ($tamanhos as $tamanho) {
                            if ($tamanho['largura'] > 0 and $tamanho['altura'] > 0) {
                                $caminho = \Config::get('upload.path');
                                $caminho .= $tamanho['path'];

                                if (!file_exists($caminho)) {
                                    \File::create_dir(\Config::get('upload.path'), $tamanho['path']);
                                }

                                /* apaga o arquivo antigo */
                                ($registro->{$file['field']} != '' and file_exists($caminho . $registro->{$file['field']})) and \File::delete($caminho . $registro->{$file['field']});

                                \Image::load($file['saved_to'] . $file['saved_as'])
                                    ->{$tamanho['method']}($tamanho['largura'], $tamanho['altura'])
                                    ->save($caminho . $file['saved_as']);
                            }
                        }
                        /* apaga o arquivo antigo, na pasta raiz */
                        ($registro->{$file['field']} != '' and $registro->{$file['field']} != $file['saved_as'] and file_exists(\Config::get('upload.path') . $registro->{$file['field']})) and \File::delete(\Config::get('upload.path') . $registro->{$file['field']});
                        $registro->{$file['field']} = $file['saved_as'];
                    }
                }
            }
        } else {
            foreach ($campos as $campo) {
                if (isset($registro->$campo)) {
                    $registro->$campo = '';
                }
            }
        }
    }

    public static function listar_linguagens($retorno = array())
    {
        $linguagens = \Config::get('site.lang');
        foreach ($linguagens as $key => $linguagem) {
            $retorno[$key] = $linguagem;
        }
        return $retorno;
    }

    /**
     * Verifica se o navegador é suportado
     * @return bool
     */
    public static function verificar_browser()
    {
        $retorno = true;

        $browser = \Agent::browser();
        $versao = \Agent::version();

        if (\Agent::is_mobiledevice() == false and $versao > 0 and \Session::get('navegador_suportado', false) == false and
            (($browser == 'IE' and $versao < 9) or ($browser == 'Chrome' and $versao < 14))) {
            $retorno = false;
        }

        return $retorno;
    }

    /* retorna o valor parcelado */
    public static function valor_parcelado($valor, $qtde)
    {
        return ceil($valor * 100 / $qtde) / 100;
    }

    public static function valores_parcelado($valor, $qtde_max_parcela, $formatado = false)
    {

        $valores = array();
        for ($parcela = 1; $parcela <= $qtde_max_parcela; $parcela++) {
            $valor_aux = $valor;
            $append = ' (sem juros)';
            if ($formatado) {
                if ($parcela == 1) {
                    $valores[$parcela] = 'Parcela única de R$ ' . static::dolar2real(static::valor_parcelado($valor_aux, $parcela)) . $append;

                } else {
                    $valores[$parcela] = $parcela . 'x de R$ ' . static::dolar2real(static::valor_parcelado($valor_aux, $parcela)) . $append;
                }
            } else {
                $valores[$parcela] = static::valor_parcelado($valor_aux, $parcela);
            }
        }
        return $valores;
    }


    /* Funções de Variáveis */
    public static function carrega_variaveis_do_usuario($id_usuario)
    {
        $vars = array(
            'variaveis' => array(),
            'valores' => array(),
        );
        $usuario = Model_Loja_Usuario::find($id_usuario);
        if ($usuario) {
            foreach (Config::get('loja.variaveis_de_email.usuario') as $var => $val) {
                $vars['variaveis'][] = $var;
                if (isset($usuario->$val['campo'])) {
                    $vars['valores'][] = $usuario->$val['campo'];
                } elseif (method_exists($usuario, $val['campo'])) {
                    $vars['valores'][] = $usuario->$val['campo'];
                } else {
                    $vars['valores'][] = $var;
                }
            }
            return $vars;
        }
        return null;
    }

    public static function carrega_variaveis_do_pedido($id_pedido)
    {
        $vars = array(
            'variaveis' => array(),
            'valores' => array(),
        );
        $pedido = Model_Loja_Pedido::find($id_pedido);
        if ($pedido) {
            foreach (Config::get('loja.variaveis_de_email.pedido') as $var => $val) {
                $vars['variaveis'][] = $var;

                if (method_exists($pedido, $val['campo'])) {
                    $vars['valores'][] = $pedido->$val['campo']();
                } elseif (isset($pedido->$val['campo'])) {
                    $vars['valores'][] = $pedido->$val['campo'];
                } else {
                    $vars['valores'][] = $var;
                }
            }
            return $vars;
        }
        return null;
    }

    public static function carrega_variaveis_do_form($form_post)
    {
        $vars = array(
            'variaveis' => array(),
            'valores' => array(),
        );
        if ($form_post) {
            foreach (Config::get('loja.variaveis_de_email.form_post') as $var => $val) {
                $vars['variaveis'][] = $var;
                if (isset($form_post[$val['campo']])) {
                    $vars['valores'][] = $form_post[$val['campo']];
                } else {
                    $vars['valores'][] = $var;
                }
            }
            return $vars;
        }
        return null;
    }

    public static function carrega_variaveis($id_usuario = null, $id_pedido = null, $form_post = null)
    {
        $vars = array(
            'variaveis' => array(),
            'valores' => array(),
        );
        if ($id_usuario) {
            $vars_usuario = FuncoesLoja::carrega_variaveis_do_usuario($id_usuario);
            $vars = array_merge_recursive($vars, $vars_usuario);
        }
        if ($id_pedido) {
            $vars_pedido = FuncoesLoja::carrega_variaveis_do_pedido($id_pedido);
            $vars = array_merge_recursive($vars, $vars_pedido);
        }
        if ($form_post) {
            $vars_form = FuncoesLoja::carrega_variaveis_do_form($form_post);
            $vars = array_merge_recursive($vars, $vars_form);
        }
        return $vars;
    }

    public static function substitui_variaveis($string, $vars)
    {
        return str_replace($vars['variaveis'], $vars['valores'], $string);
    }

    /**
     * method masks the username of an email address
     *
     * @param string $email the email address to mask
     * @param string $mask_char the character to use to mask with
     * @param int $percent the percent of the username to mask
     */
    public static function mask_email($email, $mask_char = '*', $percent = 50)
    {

        list($user, $domain) = preg_split("/@/", $email);

        $len = strlen($user);

        $mask_count = floor($len * $percent / 100);

        $offset = floor(($len - $mask_count) / 2);

        $masked = substr($user, 0, $offset)
            . str_repeat($mask_char, $mask_count)
            . substr($user, $mask_count + $offset);


        return ($masked . '@' . $domain);
    }

    /* Conversões */
    public static function dolar2real($valor)
    {
        $valor = floatval($valor);
        $valor = number_format($valor, 2, ',', '.');
        return $valor;
    }

    public static function real2dolar($valor)
    {
        $valor = str_replace(".", '', $valor);
        $valor = str_replace(",", ".", $valor);
        return $valor;
    }

    public static function date2unix($date, $hour = 0, $min = 0, $sec = 0)
    {
        $date_i = explode("/", $date);
        if (!is_array($date_i) or !isset($date_i[0]) or !isset($date_i[1]) or !isset($date_i[2]))
            return 0;
        return mktime($hour, $min, $sec, $date_i[1], $date_i[0], $date_i[2]);
    }

    /* Validações */
    public static function _validation_valid_cpf($valor)
    {
        $j = 0;
        for ($i = 0; $i < (strlen($valor)); $i++)
        {
            if (is_numeric($valor[$i]))
            {
                $num[$j] = $valor[$i];
                $j++;
            }
        }

        if (isset($num) and count($num) != 11)
        {
            $cpf_valido = false;
        }
        else
        {
            for ($i = 0; $i < 10; $i++)
            {
                if ($num[0] == $i and $num[1] == $i and $num[2] == $i and
                    $num[3] == $i and $num[4] == $i and $num[5] == $i and
                    $num[6] == $i and $num[7] == $i and $num[8] == $i)
                {
                    $cpf_valido = false;
                    break;
                }
            }
        }
        // Digito verificador 1
        if ( ! isset($cpf_valido))
        {
            $j = 10;
            for ($i = 0; $i < 9; $i++)
            {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2)
            {
                $dg = 0;
            }
            else
            {
                $dg = 11 - $resto;
            }
            if ($dg != $num[9])
            {
                $cpf_valido = false;
            }
        }
        // Digito verificador 2
        if ( ! isset($cpf_valido))
        {
            $j = 11;
            for($i = 0; $i < 10; $i++)
            {
                $multiplica[$i] = $num[$i] * $j;
                $j--;
            }
            $soma = array_sum($multiplica);
            $resto = $soma % 11;
            if ($resto < 2)
            {
                $dg = 0;
            }
            else
            {
                $dg = 11 - $resto;
            }
            if ($dg != $num[10])
            {
                $cpf_valido = false;
            }
            else
            {
                $cpf_valido = true;
            }
        }
        return $cpf_valido;
    }

    public static function _validation_valid_cnpj($valor)
    {
        $j = 0;
        for ($i = 0; $i < (strlen($valor)); $i++)
        {
            if (is_numeric($valor[$i]))
            {
                $num[$j] = $valor[$i];
                $j++;
            }
        }

        if (isset($num) and count($num) != 14)
        {
            $cnpj_valido = false;
        }

        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
        {
            $soma += $num{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($num{12} != ($resto < 2 ? 0 : 11 - $resto))
            $cnpj_valido = false;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
        {
            $soma += $num{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        if ($num{13} == ($resto < 2 ? 0 : 11 - $resto))
            $cnpj_valido = true;

        return $cnpj_valido;
    }

    public static function _validation_valid_cpf_cnpj($valor)
    {
        return (strlen($valor) > 14) ? static::_validation_valid_cnpj($valor) : static::_validation_valid_cpf($valor);
    }
    /*
    public static function _validation_unique($valor, $options)
    {
        Validation::active()->set_message('unique', 'O :label <strong>:value</strong> já está sendo utilizado.');
        list($tabela, $campo, $id) = explode(',', $options);
        $result = DB::select(\DB::expr("LOWER ($campo)"))->where($campo, '=', Str::lower($valor))->where('id', '<>', $id)->from($tabela)->execute();
        print_r($result);
        return ! ($result->count() > 0);
    }
    */

    public static function is_date($date)
    {
        static::_validation_valid_date($date);
    }

    /**
     *
     *  Gera versão automaticamente para evitar conflito
     *
     * @param $file  O Arquivo que sera carregado. Precisa ser o caminho absoluto
     * @return Nome do arquivo concatenado com a versão
     */

    public static function auto_version($file)
    {
        if (strpos($file, '/') !== 0 || !file_exists(DOCROOT . $file)) {
            return $file;
        }

        $mtime = filemtime(DOCROOT . $file);
        return preg_replace('{\\.([^./]+)$}', ".$mtime.\$1", $file);
    }

    public static function remover_caracter($string)
    {
        $string = preg_replace("/[ÁÀÂÃÄáàâãä]/", "a", $string);
        $string = preg_replace("/[ÉÈÊéèê]/", "e", $string);
        $string = preg_replace("/[ÍÌíì]/", "i", $string);
        $string = preg_replace("/[ÓÒÔÕÖóòôõö]/", "o", $string);
        $string = preg_replace("/[ÚÙÜúùü]/", "u", $string);
        $string = preg_replace("/Çç/", "c", $string);
        $string = preg_replace("/[][><}{)(:;,!?*%~^`@]/", "", $string);
        $string = preg_replace("/ /", "_", $string);
        return $string;
    }

    public static function is_md5($md5 ='')
    {
        return preg_match('/^[a-f0-9]{32}$/', $md5);
    }
}
