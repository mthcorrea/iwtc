<?php

/*
* Classe de paginação para javascript
* 
*/

class Clickpagination
{
	/**
	 * @var	array	Pagination instances
	 */
	protected static $_instances = array();

	/**
	 * @var	array	Pagination default instance
	 */
	protected static $_instance = null;

	/**
	 * Init
	 *
	 * Loads in the config and sets the variables
	 *
	 * @return	void
	 */
	public static function _init()
	{
	}

	/**
	 * Static access to the default instance
	 *
	 * @return	mixed
	 * @throws	BadMethodCallException if the request method does not exist
	 */
	public static function __callStatic($name, $arguments)
	{
		// old pre-1.4 mapping to new instance methods
		static $mapping = array(
			'get'           => '__get',
			'set'           => '__set',
			'set_config'    => '__set',
			'create_button' => 'render',
		);

		array_key_exists($name, $mapping) and $name = $mapping[$name];

		// call the method on the default instance
		if ($instance = static::instance() and method_exists($instance, $name))
		{
			return call_fuel_func_array(array($instance, $name), $arguments);
		}

		throw new \BadMethodCallException('The pagination class doesn\'t have a method called "'.$name.'"');
	}

	/**
	 * forge a new pagination instance
	 *
	 * @return	\Pagination	a new pagination instance
	 */
	public static function forge($name = 'default', $config = array())
	{
		if ($exists = static::instance($name))
		{
			\Error::notice('Pagination with this name exists already, cannot be overwritten.');
			return $exists;
		}

		static::$_instances[$name] = new static($config);

		if ($name == 'default')
		{
			static::$_instance = static::$_instances[$name];
		}

		return static::$_instances[$name];
	}

	/**
	 * retrieve an existing pagination instance
	 *
	 * @return	\Pagination	a existing pagination instance
	 */
	public static function instance($name = null)
	{
		if ($name !== null)
		{
			if ( ! array_key_exists($name, static::$_instances))
			{
				return false;
			}

			return static::$_instances[$name];
		}

		if (static::$_instance === null)
		{
			static::$_instance = static::forge();
		}

		return static::$_instance;
	}

	// --------------------------------------------------------------------

	/**
	 * instance configuration values
	 */
	protected $config = array(
		'current_page'            => null,
		'offset'                  => 0,
		'per_page'                => 10,
		'total_pages'             => 0,
		'total_items'             => 0,
		'num_links'               => 5,
		'uri_segment'             => 3,
		'pagination_url'          => null,
	);

	/**
	 * raw pagination results
	 */
	protected $raw_results = array();

	/**
	 *
	 */
	public function __construct($config = array())
	{
		// make sure config is an array
		is_array($config) or $config = array('name' => $config);

		// and we have a template name
		array_key_exists('name', $config) or $config['name'] = \Config::get('pagination.active', 'default');

		// merge the config passed with the defined configuration
		$config = array_merge(\Config::get('pagination.'.$config['name'], array()), $config);

		// don't need the template name anymore
		unset($config['name']);

		// update the instance default config with the data passed
		foreach ($config as $key => $value)
		{
			$this->__set($key, $value);
		}
	}

	/**
	 * configuration value getter
	 */
	public function __get($name)
	{
		// use the calculated page if no current_page is passed
		if ($name === 'current_page' and $this->config[$name] === null)
		{
			$name = 'calculated_page';
		}

		if (array_key_exists($name, $this->config))
		{
			return $this->config[$name];
		}
		else
		{
			return null;
		}
	}


	/**
	 * configuration value setter
	 */
	public function __set($name, $value = null)
	{
		if (is_array($name))
		{
			foreach($name as $key => $value)
			{
				$this->__set($key, $value);
			}
		}
		else
		{
			if (array_key_exists($name, $this->config))
			{
				$this->config[$name] = $value;
			}
		}

		// update the page counters
		$this->_recalculate();
	}

	/**
	 * Create button for pagination
	 *
	 */
	public function render()
	{
		$attr['class'] = 'btn btn-default btn-block click-pagination';

		if ($this->config['total_pages'] == 1)
		{
			return '';
		}
		
		if ($this->config['calculated_page'] == $this->config['total_pages'])
		{
			$attr['disabled'] = 'disabled';
			$value = 'Não há mais itens';
		}
		else
		{
			$value = 'Exibir mais itens';
		}

		return \Form::button('click-pagination', $value, $attr);
	}


	/**
	 * Prepares vars for creating links
	 */
	protected function _recalculate()
	{
		// calculate the number of pages
		$this->config['total_pages'] = (int) ceil($this->config['total_items'] / $this->config['per_page']) ?: 1;

		// get the current page number, either from the one set, or from the URI or the query string
		if ($this->config['current_page'])
		{
				$this->config['calculated_page'] = $this->config['current_page'];
		}
		elseif (\Input::is_ajax() === true)
		{
			if (is_string($this->config['uri_segment']))
			{
				$this->config['calculated_page'] = \Input::post($this->config['uri_segment'], 1);
			}
			else
			{
				$this->config['calculated_page'] = (int) \Request::main()->uri->get_segment($this->config['uri_segment']);
			}
		}
		else
		{
			$this->config['calculated_page'] = 1;
		}

		// make sure the current page is within bounds
		if ($this->config['calculated_page'] < 1)
		{
			$this->config['calculated_page'] = 1;
		}

		// the current page must be zero based so that the offset for page 1 is 0.
		$this->config['offset'] = ($this->config['calculated_page'] - 1) * $this->config['per_page'];
	}

}
