<?php

namespace Fuel\Migrations;

class Create_contato
{
	public function up()
	{
        \DBUtil::create_table('contato', array(
            'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'fk_departamento' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'unsigned' => true),
			'fk_vaga' => array('constraint' => 11, 'type' => 'int', 'null' => true, 'unsigned' => true),
            'tipo' => array('constraint' => 4, 'type' => 'tinyint', 'null' => true, 'default' => 0),
            'nome' => array('constraint' => 150, 'type' => 'varchar', 'null' => true),
            'email' => array('constraint' => 200, 'type' => 'varchar', 'null' => true),
            'telefone' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
            'assunto' => array('constraint' => 150, 'type' => 'varchar', 'null' => true),
            'mensagem' => array('type' => 'text', 'null' => true),
            'arquivo' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
            'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
            'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
        ), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('contato');
	}
}
