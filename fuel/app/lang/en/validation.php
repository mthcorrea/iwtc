<?php

return array(
	'required'        => 'O campo ":label" é obrigatório e deve conter algum valor.',
	'min_length'      => 'O campo ":label" deve conter no mínimo :param:1 caracteres.',
	'max_length'      => 'O campo ":label" não deve conter mais que :param:1 caracteres.',
	'exact_length'    => 'O campo ":label" deve conter exatamente :param:1 caracteres.',
	'match_value'     => 'O campo ":label" deve conter o valor :param:1.',
	'match_pattern'   => 'O campo ":label" deve seguir o padrão :param:1.',
	'match_field'     => 'O campo ":label" deve ser igual ao campo :param:1.',
	'valid_date'      => 'O campo ":label" deve conter uma data válida.',
	'valid_email'     => 'O campo ":label" deve conter um endereço de e-mail válido.',
	'valid_emails'    => 'O campo ":label" deve conter uma lista de endereços de e-mail válido.',
	'valid_url'       => 'O campo ":label" deve conter uma URL válida.',
	'valid_ip'        => 'O campo ":label" deve conter um endereço de IP válido.',
	'numeric_min'     => 'O valor mínimo numérico do campo ":label" deve ser :param:1.',
	'numeric_max'     => 'O valor máximo numérico do campo ":label" deve ser :param:1.',
	'numeric_between' => 'O campo ":label" deve conter um número entre :param:1 e :param:2',
	'valid_string'    => 'A regra :rule para o campo ":label" falhou.',
	'required_with'   => 'O campo ":label" é obrigatório se :param:1 estiver preenchido.',
	'unique_url'      => 'O campo ":label" já existe em nosso banco de dados.',
    'valid_cpf_cnpj'  => 'O campo ":label" não é válido.',
);
