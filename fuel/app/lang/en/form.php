<?php

return array(
    'sem_registro' => array(
        'label' => 'Nenhum item encontrado.',
    ),
    'nome' => array(
        'label' => 'Nome',
    ),
	'cargo' => array(
		'label' => 'Cargo',
	),
	'departamento' => array(
		'label' => 'Departamento',
	),
    'email' => array(
        'label' => 'E-mail'
    ),
    'telefone' => array(
        'label' => 'Telefone'
    ),
    'assunto' => array(
        'label' => 'Assunto'
    ),
    'mensagem' => array(
        'label' => 'Mensagem'
    ),
    'arquivo' => array(
        'label' => 'Arquivo'
    ),
    'quantidade' => array(
        'label' => 'Quantidade'
    ),
    'produto' => array(
        'label' => 'Produto'
    ),
    'acoes' => array(
        'label' => 'Ações'
    ),
    'razao_social' => array(
        'label' => 'Razão Social / Nome Fantasia'
    ),
    'documento' => array(
        'label' => 'CNPJ'
    ),
    'inscricao_estadual' => array(
        'label' => 'Inscrição Estadual'
    ),
    'endereco' => array(
        'label' => 'Endereço Completo'
    ),
    'cidade' => array(
        'label' => 'Cidade'
    ),
    'estado' => array(
        'label' => 'UF'
    ),
    'observacao' => array(
        'label' => 'Observações'
    ),
    'revestimento' => array(
        'label' => 'Revestimento'
    ),
    'dn_tubo' => array(
        'label' => 'DN. do Tubo'
    ),
    'dn' => array(
        'label' => 'DN'
    ),
    'tamanho' => array(
        'label' => 'Tamanho (Ponta a Ponta)'
    ),
    'conexao_a' => array(
        'label' => 'Conexão A'
    ),
    'conexao_b' => array(
        'label' => 'Conexão B'
    ),
    'pressao' => array(
        'label' => 'Pressão'
    ),
    'temperatura' => array(
        'label' => 'Temperatura'
    ),
    'aplicacao' => array(
        'label' => 'Aplicação'
    ),
    'buscar' => array(
        'placeholder' => 'Realize sua Busca'
    ),
    'enviar' => array(
        'text' => 'Enviar',
        'attr' => 'Carregando'
    ),
    'btn_especificar' => array(
        'titulo' => 'Ver Especificação',
        'attr' => 'Carregando'
    ),
    'btn_continuar' => array(
        'titulo' => 'Continuar Selecionado Produtos',
    ),
    'btn_orcamento' => array(
        'titulo' => 'Enviar Solicitação de Orçamento',
        'attr' => 'Carregando'
    ),
    'btn_atualizar' => array(
        'titulo' => 'Atualizar',
        'attr' => 'Carregando'
    ),
    'btn_adicionar' => array(
        'titulo' => 'Adicionar ao Carrinho',
        'attr' => 'Adicionando'
	),
	'anexo' => array(
		'label' => 'Anexar arquivo caso necessário (DOC, PPT ou PDF)',
		'curriculo' => 'Anexar currículo (DOC, PPT ou PDF)',
	),
	'descricao' => '*Especificações não obrigatórias, descreva as especificações ou clique direto no botão “Adicionar ao Carrinho”.',
);
