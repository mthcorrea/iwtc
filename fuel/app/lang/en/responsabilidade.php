<?php

return array(
    'header' => array(
        'secao' => 'Responsabilidade',
        'titulo' => 'Onde tem <span>desafio, </span><br>tem <span>Sendi</span>',
    ),
    'topicos' => array(
        'seguranca' => 'Segurança',
        'seguranca-texto' => 'Investimos constantemente em treinamentos de conscientização e educação em relação ao meio-ambiente, segurança no trabalho e ações sustentáveis.  A aplicação para a capacitação, cuidados e melhorias contínua são desenvolvidos por meio de atividades lúdicas e interativas, para transformar a conscientização em um processo motivador e objetivo comum a todos.',
            'inovacao' => 'Inovação',
            'inovacao-texto' => 'Sustentar processos criativos com foco e visão de futuro exigem a evolução de ideias, conceitos e novas atitudes. Por isso vemos em cada projeto um novo desafio, porém uma oportunidade para inovar. Desta forma buscamos sempre oferecer as melhores e mais adequadas soluções, respeitando o desenvolvimento sustentável e as boas práticas de gestão e desenvolvimento.',
                'trabalho_equipe' => 'Trabalho em Equipe',
                'trabalho_equipe-texto' => 'Nenhum trabalho chega ao seu ápice de reconhecimento sem que as pessoas e equipes envolvidas dê o seu melhor. Do chão de fábrica à liderança administrativa, o espírito de equipe é o que guia cada projeto. Por isso, as nossas soluções oferecidas são sempre de excelência e honram compromissos.',
                    'superacao_desafios' => 'Superação de Desafios',
                    'superacao_desafios-texto' => 'Superar desafios é o que nos move, por isso apoiamos iniciativas que também compartilham desta filosofia, colocando em prática a busca incansável por excelência e resultados.',
        'texto-bottom' => 'Gerenciamos nossos pilares tendo em mente as estratégias de negócio, seguindo o nosso core business. Por isso nossa missão e valores estão em todas as nossas ações e atitudes. Desta forma, é possível reforçar o nosso compromisso junto à sociedade',
    ),
    'patrocinados' => array(
        'patrocinio1' => array(
            'tipo' => 'Basket',
            'titulo' => 'Bauru <span>Basket</span>',
            'texto' => 'A Sendi patrocina o time de basquete da cidade de Bauru porque acredita no poder do Trabalho em Equipe e na Superação de Desafios. Esse apoio visa estimular o reconhecimento da empresa como um time, em que a união de potenciais e a coragem de enfrentar obstáculos transformam qualquer resultado. O esporte promove essa reflexão e ainda oferece muito entretenimento cultural.',
        ),
        'patrocinio2' => array(
            'tipo' => 'Kart',
            'titulo' => 'Equipe de <span>Kart</span>',
            'texto' => 'Superar desafios é um dos grandes objetivos da Sendi, por isso ao identificar características de liderança, dinamismo, força, superação e tecnologia no automobilismo, a empresa passa a incentiva-lo. Em Bauru, a Sendi investe no Kart por ser um esporte que ultrapassa qualquer obstáculo, estimula a evolução constante e alcança a glória.',
        ),
    ),
);