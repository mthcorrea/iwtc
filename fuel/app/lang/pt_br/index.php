<?php

return array(
    'a_sendi' => array(
    	'titulo'=> 'A Sendi',
    	'subtitulo' => 'Paixão pelo <br><span>desafio</span>',
    	'texto' => 'Uma base sólida que transforma conhecimento e experiência em entregas de alta qualidade, de acordo com as normas técnicas e boas práticas da engenharia, atendendo com expectativa e satisfação total os nossos clientes.',
    	'botao' => array(
			'texto' => 'SAIBA MAIS SOBRE A SENDI&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => 'a-sendi',
		),
		'orcamento' => array(
			'texto' => 'Estamos por trás de grandes desafios, que através de soluções específicas e inovadoras resultam em grandes obras que levam infraestrutura e desenvolvimento para todo o Brasil.',
			'botao' => array(
				'texto' => 'Estamos por trás de grandes desafios, que através de soluções específicas e inovadoras resultam em grandes obras que levam infraestrutura e desenvolvimento para todo o Brasil.',
				'link' => 'SOLICITE UM ORÇAMENTO',
				'url_amigavel' => 'contato',
			),
		)
    ),
    'obras' => array(
    	'titulo' => 'Obras',
		'subtitulo' => '<span>Qualidade Sendi</span><br> sempre presente',
		'texto' => 'Conheça algumas das grandes obras que levam o padrão Sendi de qualidade para todo o Brasil.',
		'botao' => array(
			'texto' => 'VEJA TODAS NOSSAS OBRAS&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => 'obras',
		),
    ),
    'noticias' => array(
    	'titulo' => 'NOTÍCIAS',
    	'subtitulo' => 'Acompanhe <span>as novidades</span>',
    	'leia_mais' => 'Leia mais +',
		'todas' => array(
			'texto' => 'VEJA TODAS AS NOTÍCIAS&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
			'url_amigavel' => 'noticias'
		)
	)
);
