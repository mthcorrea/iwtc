<?php

return array(
	'fale_conosco' => array(
		'label' => 'Fale conosco',
	),
	'entre_contato' => array(
		'label' => 'Entre em <span>contato</span>',
	),
	'departamento_info' => array(
		'label' => 'Escolha o departamento da empresa e envie sua mensagem.',
	),
	'departamento' => array(
		'label' => 'Departamento',
	),
	'nome' => array(
		'label' => 'Nome',
	),
	'email' => array(
		'label' => 'E-mail',
	),
	'telefone' => array(
		'label' => 'Telefone',
	),
	'assunto' => array(
		'label' => 'Assunto',
	),
	'mensagem' => array(
		'label' => 'Mensagem',
	),
	'anexo' => array(
		'label' => 'Anexar arquivo caso necessário (DOC, PPT ou PDF)',
		'curriculo' => 'Anexar currículo (DOC, PPT ou PDF)',
	),
	'enviar' => array(
		'label' => 'Enviar',
	),
	'trabalhe_conosco' => array(
		'label' => '<span>Trabalhe</span> conosco',
	),
	'trabalhe_conosco-info' => array(
		'label' => 'Cadastre agora mesmo o seu contato para futuras oportunidades, ou candidate-se a uma de nossas vagas disponíveis. Preencha o formulário retornaremos o contato de acordo com o seu perfil.',
	),
	'vaga' => array(
		'label' => 'Vaga',
		'selecionar' => 'Vaga: <br>(selecione uma vaga específica ou deixe em branco para se cadastrar no nosso banco de dados)',
	),
	'local' => array(
		'label' => 'Local',
	),
	'area' => array(
		'label' => 'Área',
	),
	'candidatar' => array(
		'label' => 'Quero me candidatar para essa vaga&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right" aria-hidden="true"></i>',
	),
	'complemente' => array(
		'label' => 'Complemente as informações sobre você',
	),
);
